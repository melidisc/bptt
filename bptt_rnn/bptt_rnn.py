import numpy as np
import theano
import theano.tensor as T
import time
from random import shuffle
import sys

def shared_normal(num_rows, num_cols, scale=1):
    '''Initialize a matrix shared variable with normally distributed
elements.'''
    return theano.shared(np.random.normal(
        scale=scale, size=(num_rows, num_cols)).astype(theano.config.floatX))

def shared_zeros(*shape):
    '''Initialize a vector shared variable with zero elements.'''
    return theano.shared(np.zeros(shape, dtype=theano.config.floatX))

def shared_ones(*shape):
    '''Initialize a vector shared variable with zero elements.'''
    return theano.shared(np.ones(shape, dtype=theano.config.floatX))

def construct_inp(value, range):
	if type(value) is int:
		tmp = np.zeros(range)
		tmp[value] = 1
		return np.array([tmp])
	else:
		return value

class Net:
	def __init__(self, output_s, input_s, epoch_size, output_tap=[], lr=0.002, pb=[]):

		#internal variables
		self.unit_s = output_s
		self.input_s = input_s
		self.unit_tr = output_tap
		self.lr = lr
		self.bias = True if pb!=[] else False
		self.epoch = epoch_size
		
		#buffers
		self.input_buff = shared_zeros(self.epoch,self.input_s)
		self.unit_val_buff = shared_zeros(self.epoch,self.unit_s)
		self.error_buff = shared_zeros(self.epoch,self.input_s)

		#if we have biases, include them in the input
		if pb !=[]:
			self.inputs_s += len(pb)

		# Weight matrix [units X (units + inputs)]
		self.weights_r = shared_normal(self.unit_s,self.unit_s,0.01)
		self.weights_i = shared_normal(self.input_s,self.unit_s,0.01)
		
		self.params = self.weights_r, self.weights_i
		#initial values for the recurrent units
		self.units_val = shared_zeros(self.unit_s)

		tmp = np.zeros(self.unit_s, dtype=theano.config.floatX)
		tmp[:self.input_s]  = 1
		self.mask = theano.shared(tmp)
	# def reinit(self,):
	# 	#buffers
	# 	return theano.function(
	# 		inputs=[],
	# 		outputs=[],
	# 		updates=[
			
	# 		])

	def calc_units_f(self,):
		ceps = T.iscalar("epoch_number")
		inp = T.vector("inp")
		ito = T.dot(inp, self.weights_i)
		oto = T.dot(self.units_val, self.weights_r)
		# self.units_val.set_values( ito + oto)
		
		return theano.function(
			inputs=[inp,ceps], 
			outputs = [self.units_val,oto,ito], 
			updates=[
			(self.units_val, ito+oto),
			(self.input_buff, T.set_subtensor(self.input_buff[ceps],inp)),
			(self.unit_val_buff, T.set_subtensor(self.unit_val_buff[ceps],self.units_val))
			])

	# def simple_pass(self,):
	# 	inp = T.vector("input")
	# 	ito = T.dot(inp, self.weights_i)
	# 	oto = T.dot(self.units_val, self.weights_r)
	# 	self.units_val = ito + oto
	
	# 	return theano.function(
	# 		inputs=[inp], 
	# 		outputs = [T.nnet.sigmoid(ito + oto)]
	# 		)

	def calc_out_f(self,check=False):
		
		out_val = T.vector("out_val")
		if check:
			'''
				Simple pass of the net
			'''
			return theano.function(
				inputs=[out_val], 
				outputs = [T.nnet.sigmoid(out_val)])
		else:
			'''
				Pass to facilitate learnign, updating buffers and 
			'''
			ceps = T.iscalar("epoch_number")
			target  = T.vector("target")
			outou = T.nnet.sigmoid(out_val)
			error = (target - outou[:self.input_s])#*self.mask
		
			return theano.function(
				inputs=[out_val, target, ceps], 
				outputs = [outou, (error**2).sum(),error],
				updates=[
				(self.error_buff, T.set_subtensor(self.error_buff[ceps], error))
				])

	def bprop_epoch(self,):
		

		'''
			Give it error at t1 and run recursively. 
			It will give you the epsilons for t0 to t1
			need to have 
				the activations of units for all times tau (t0,t1]
				the errors for times (t0,t1]
			also 
				for the first iteration => epsilon_tau = epsilon_t1 = error_t1 
			!!!GENERAL: we need the sequence of inputs presented
		'''
		def epsilon_tau( error_tau_1minus, error_tau, units_val, units_val_tau1minus, inp_tau_1minus, w_r, w_i, e_tau,*_):
			d_tau  = T.nnet.sigmoid(units_val)*(1-T.nnet.sigmoid(units_val))* e_tau
			
			w_r +=  d_tau.T * T.nnet.sigmoid(units_val_tau1minus)
			w_i +=  d_tau.T * inp_tau_1minus

			return w_r, w_i, error_tau_1minus + T.dot(d_tau, self.weights_r)

		result, update = theano.scan(fn=epsilon_tau,
									outputs_info=[
									dict(initial= T.zeros((self.unit_s,self.unit_s),dtype=theano.config.floatX) , taps=[-1]),
									dict(initial= T.zeros((self.input_s,self.unit_s),dtype=theano.config.floatX) , taps=[-1]), 
									dict(initial= self.error_buff[-1] , taps=[-1])],
									sequences=[
										dict(input = self.error_buff , taps=[-2,-1]),
										dict(input = self.unit_val_buff , taps=[-2,-1]), 
										dict(input= self.input_buff , taps=[-2])],
									non_sequences=[self.weights_r, self.weights_i], 
									go_backwards=True,
									name="Scan Gradient",
									)
		
		
		return theano.function(
			inputs=[], 
			outputs=[result[-1], self.error_buff], 
			updates=[
			#update,
			(self.weights_r, self.weights_r+(result[-1][0] * self.lr)),
			(self.weights_i, self.weights_i+(result[-1][1] * self.lr)),
			(self.input_buff , T.zeros((self.epoch,self.input_s),dtype=theano.config.floatX)),
			(self.unit_val_buff , T.zeros((self.epoch,self.unit_s),dtype=theano.config.floatX)),
			(self.error_buff , T.zeros((self.epoch,self.input_s),dtype=theano.config.floatX)),
			(self.units_val , T.zeros(self.unit_s,dtype=theano.config.floatX))
			])
	
if __name__ == "__main__":

	out_size = 11
	in_size = 6
	epoch_size  = 50
	repetathions = 10000
	
	net = Net(out_size, in_size, epoch_size, lr=0.0005)
	
	#create the random input stream, 1000 inpus 
	# ins = (np.random.random((6, 1000))*in_size).astype(int)
	# tars = (np.zeros(((6, 1000),out_size))).astype(int)
	# targets = (np.random.random((4,6))*in_size).astype(int)
	
	#compile the unit value function
	units = net.calc_units_f()
	
	#compile the output value function
	out_v = net.calc_out_f()
	
	#compile reinit for reinitialization after each epoch
	# reinit = net.reinit()
	
	#compile the grad calculate function
	grad_calc = net.bprop_epoch()
	
	# compile for a simple net pass without error accumulation
	out_v_no_learn = net.calc_out_f(check=True)

	t0= time.time()	
	
	import pickle

	f = open("6D_moves.pickle", "rb")

	targets = pickle.load(f)
	# [
	# 	(np.random.random((6,100))+np.sin(np.linspace(-np.pi, np.pi, 100))).reshape(100,6),
	# 	(np.random.random((6,110))+np.cos(np.linspace(-3*np.pi, 3*np.pi, 110))).reshape(110,6),
	# 	(np.random.random((6,150))+np.cos(np.linspace(-np.pi/2, np.pi/2, 150))).reshape(150,6)
	# 	]
	
	for _ in xrange(repetathions):
		print "idx ", _,
		for t, target in enumerate(targets):
			for i in range(len(target)):
				
				# waht to eat
				inp = target[i].astype(theano.config.floatX) 
				# print "in shape ", inp.shape

				# chew 
				vals = units(inp, i%epoch_size)
				# print np.asarray(vals[0]).shape	

				# what to spit
				out = target[(i+1)%len(target)]

				# eat and swallow
				output = out_v(vals[0], out.astype(theano.config.floatX), i%epoch_size) 
				print output[-1]

				# digest
				if i%epoch_size == 0 or i==len(target):
					gc = grad_calc()
			# show me
			print " target ", t, "MSE ", output[1], #"\r",#  "error len ",output[2].shape#," oto ",vals[1], #,"\r",
		print "\r"
















	'''
	#train
	for _ in xrange(100):
		a_seen = False
		for idx, z in enumerate(zip(ins,tars)):
			try:
				inp = z[0]
				tar = z[1]

				if inp == 1:
					a_seen = True
				if inp == 2 and a_seen:
					tar[1] = 1		
					a_seen = False
				
				net_inp = construct_inp(
						inp, 
						range=in_size)[0].astype(theano.config.floatX)
				
				vals = units(net_inp,idx%epoch_size)
				output = out_v(vals[0],(tar).astype(theano.config.floatX),idx%epoch_size) 
				if idx%epoch_size == 0:
					res = grad_calc()
					# print net.unit_val_buff.get_value()
					# print net.error_buff.get_value()
					# print net.input_buff.get_value()
					# reinit()
			except KeyboardInterrupt: 
				print "wr ",res[0][0]
				print "wi ",res[0][1]
				print "Dtau ",res[0][2]
				sys.exit()
			 
				
		print "MSE ", output[1], " idx ", _," error len ",output[2].shape," oto ",vals[1] #,"\r",
		
	#test

	a_seen = False
	ins = (np.random.random(1000)*in_size).astype(int)
	tars = (np.zeros((1000,out_size))).astype(int)

	for idx,z in enumerate(zip(ins,tars)):
		inp, tar= z
		if inp == 1:
			a_seen = True
		if inp == 2 and a_seen:
			tar[1] = 1		
			a_seen = False
		
		net_inp = construct_inp(
				inp, 
				range=in_size)[0].astype(theano.config.floatX)

		vals = units(net_inp,0)
		output = out_v_no_learn(vals[0]) 
		print output[0][:2], tar[:2], net_inp, vals[1], vals[2]

	
	for _ in xrange(5000):
		guesses	 = []
		for idx,target in enumerate(targets):
			target_guess = []
			for i in xrange(epoch_size):
				
				inp = construct_inp(
					target[i%len(target)], 
					range=in_size)[0].astype(theano.config.floatX)

				net_target = construct_inp(
					target[(i+1)%len(target)], 
					range=out_size)[0].astype(theano.config.floatX)
				
				vals = units(inp,i)
				output = out_v(vals,net_target,i) 
				target_guess.append(
					{
					"in":target[i%len(target)],
					"out":np.argmax(output[0][:in_size]),
					"target":target[(i+1)%len(target)]}
					)
			guesses.append(target_guess)
			out = grad_calc()	
		print "MSE ", output[1], " epoch ", _, " d_tau ", out[0][2], "\n\t weights_r ", out[0][0] , "\n\t weights_i ", out[0][1] ,"\r", 
		np.random.shuffle(targets)
	print "MSE ", output[1]

	for gs in guesses:
		for g in gs:
			print "Guess\n\t In ", g['in'], "\t"
			print " target ", g['target'],
			print " out ", g['out'], g['target']==g['out']
			print "~~~~~~~~~~~"
	
	guesses = []
	for target in targets:
		target_guess=[]
		for i in target:
			inp = construct_inp(
						i, 
						range=in_size)[0].astype(theano.config.floatX)

			vals = np.asarray(units(inp,0))
			output = np.argmax(np.asarray(out_v_no_learn(vals))[0,:in_size])

			target_guess.append(output)
		guesses.append(target_guess)

	for g,t in zip(guesses,targets):
		
		print "Target ", t
		print "Guess ", g
		print "~~~~~~~~~~~"
	'''
	print "OK", time.time() - t0

