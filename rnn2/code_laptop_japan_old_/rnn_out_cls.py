import numpy as np
import theano
import theano.tensor as TT
import sys, os
import pickle

def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])
class RNN_BPTT():
    def __init__(self, nh, ni, no, noc):
        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # numbe of reccuring ouputs
        self.noc = noc
        
        # input (where first dimension is time)
        self.u = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # target (where first dimension is time)
        self.outc = TT.matrix()
        
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # initial hidden state of the RNN
        self.outc0 = TT.vector()
        
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()

        
        # input bias
        self.inb = theano.shared(np.ones((1)).astype(theano.config.floatX))
        # output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.W = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.00001, high=.00001).astype(theano.config.floatX))
        # recurrent output weights as a shared variable
        # self.W_outc_h = theano.shared(np.random.uniform(size=(self.noc, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # recurrent output weights as a shared variable
        # self.W_h_outc = theano.shared(np.random.uniform(size=(self.n, self.noc), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(np.random.uniform(size=(self.nin+self.noc, self.n), low=-.00001, high=.00001).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_inb = theano.shared(np.random.uniform(size=(1, self.n), low=-.00001, high=.00001).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(np.random.uniform(size=(self.n, self.nout+self.noc), low=-.00001, high=.00001).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(np.random.uniform(size=(1, self.nout+self.noc), low=-.00001, high=.00001).astype(theano.config.floatX))
        # self.W_outcb = theano.shared(np.random.uniform(size=(1, self.noc), low=-.1, high=.1).astype(theano.config.floatX))

        # recurrent moemntum weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin+self.noc, self.n)).astype(theano.config.floatX))
        # input bias momentum
        self.mW_inb = theano.shared(np.zeros((1, self.n)).astype(theano.config.floatX))
        # in class to hidder momentum
        # self.mW_outc_h = theano.shared(np.zeros((self.noc, self.n)).astype(theano.config.floatX))
        # hidden to out class
        # self.mW_h_outc = theano.shared(np.zeros((self.n, self.noc)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout+self.noc)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout+self.noc)).astype(theano.config.floatX))
        # self.mW_outcb = theano.shared(np.zeros((1, self.noc)).astype(theano.config.floatX))

        ##USED IN THE BATCH NET UPDATE
        # recurrent weights as a shared variable
        self.gW = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.gW_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, outc_tm1, W, W_in, W_pb, W_out, W_outb, outb, inb):
            u_t = TT.set_subtensor(u_t[-self.noc:], outc_tm1)
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(inb, W_pb) + TT.dot(h_tm1, W))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb) )
            outc_t = y_t[-self.noc:]
            return h_t, y_t, outc_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y, outc], _ = theano.scan(step,
                                sequences=[self.u],
                                outputs_info=[self.h0, None, self.outc0],
                                non_sequences=[self.W, self.W_in, self.W_inb, self.W_out, self.W_outb, self.outb, self.inb])
        return h,y,outc
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        h, y, outc = self._inner_step()
        
        # error between output and target
        error = ((y - self.t) ** 2).sum()

        # gradients on the weights using BPTT
        gW, gW_in, gW_inb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_inb, self.W_out, self.W_outb])

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.h0, self.outc0, self.u, self.t, self.lr, self.mom],
                        [error, y, outc[-1], h[-1]],
                        updates={(self.mW, self.mom*self.mW - self.lr * gW),
                                # (self.mW_outc_h, self.mom*self.mW_outc_h - self.lr * gW_outc_h),
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_inb, self.mom*self.mW_inb - self.lr * gW_inb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                # (self.mW_h_outc, self.mom*self.mW_h_outc - 100*self.lr * gW_h_outc),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                # (self.mW_outcb, self.mom*self.mW_outcb - self.lr * gW_outcb),
                                (self.W, self.W + self.mW),
                                # (self.W_outc_h, self.W_outc_h + self.mW_outc_h),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_inb, self.W_inb + self.mW_inb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                # (self.W_outcb, self.W_outcb + self.mW_outcb),
                                (self.W_out, self.W_out + self.mW_out)}
                                # (self.W_h_outc, self.W_h_outc + self.mW_h_outc)}
                        )

    def _net_step(self,):
        # make the inner step
        h,y,outc = self._inner_step()
        
        # # calulate error
        # error = ((y - self.t) ** 2).sum()

        # compile and return the function
        return theano.function([self.h0, self.outc0, self.u],
                        [y])

    def _load_weights(self, filename):
        import pickle
        f = open(filename+".pickle","rb")

        net_w = pickle.load(f)
    
        self.W.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_outc_h.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_in.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_pb.set_value(
            np.asarray(net_w[3].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[4].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[5].get_value()))
        self.W_h_outc.set_value(
            np.asarray(net_w[6].get_value()))


        f.close()
def net_train(net, dtsets, repetathions):

    for targets in dtsets[:-1]:
        
        targets = _normalise(targets)

        target_out = []
        for t in targets:
            target_out.append(
                np.append(t[1:],t[0]).reshape(t.shape)
                )
        
        for _ in xrange(repetathions):
                
            print " Rep ", _,
            

            ov_er = 0
            for idx, stuff in enumerate(zip(targets, target_out)):
                h = np.zeros(hiden_size).astype(theano.config.floatX)
                outc = np.zeros(outc_size).astype(theano.config.floatX)
                i, o =  stuff

                pb = np.zeros(len(targets))
                pb[idx] = 1

                oo = []
                for outt in o:
                    oo.append(
                        np.append(outt, pb)
                        )
                o = np.array(oo)
                ii = []
                for inn in i:
                    ii.append(
                        np.append(inn, np.zeros(len(targets)))
                        )
                i = np.array(ii)
                # pb = np.tile(pb, i.shape[0]).reshape(i.shape[0], len(targets))

                what = net_trainer_accumulator(
                    h,
                    outc, 
                    i, 
                    o,
                    0.00021,
                    0.0
                    )
                outc = what[-2]
                h = what[-1]
                # print "\nMSE _",idx,"_", what[0], " \n", outc, pb
                ov_er+= what[0]

            print "Overal error ", ov_er
            

def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array, filename):
    
    f = open("net_trained_"+filename+".pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()
def _normalise(targets):
    print "Normalising values ", len(targets), len(targets[0])
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss
    return targets

if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print "Usage "+sys.argv[0]+" [dirname] [extension] [file _load_weights] [file save_weights]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])

    out_size = 6
    in_size = 6
    outc_size = len(dtsets[0])
    hiden_size = 11

    repetathions = 50000

    net = RNN_BPTT(hiden_size, in_size, out_size, outc_size)
    if sys.argv[3] != "no":
        net._load_weights(sys.argv[3])
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    print "NET CONF ",hiden_size, in_size, out_size, outc_size

    if sys.argv[3] == "no":
        print "Training"
        net_train(net, dtsets, repetathions)
    else:
        print "Whaaat"
        import matplotlib.pyplot as plt
    
    targets = _normalise(dtsets[-1])
    target_out = []
    for t in targets:
        target_out.append(
            np.append(t[1:],t[0]).reshape(t.shape)
            )

    if sys.argv[4] != "no":
        save_weights([net.W, net.W_h_outc, net.W_in, net.W_inb, net.W_out, net.W_outb, net.W_outc_h], sys.argv[4])

    
    succes_rate=np.zeros(len(targets))
    
    pbz_plot = []
    pbs_plot = []
    out_plot = []
    
    h = np.zeros(hiden_size).astype(theano.config.floatX)
    outc = np.zeros(outc_size).astype(theano.config.floatX)
    
    for idx, stuff in enumerate(zip(targets, target_out)):
        
        i, o =  stuff
        
        what = net_trainer_stepper(h,outc,i)
        
        error, y, outc = what
        print "outc ",outc
        print "error", error
        # print succes_rate, " of ", [len(t) for t in targets]

        if sys.argv[3] != "no":
            plt.figure()
            plt.ylabel('Values Seq'+str(idx))
            plt.xlabel('Time')
            plt.plot(pbs_plot[-len(o):],"--",label="Pb")
            plt.plot(o,"-")
            plt.legend()
            # plt.figure()
            # plt.plot(out_plot,"-", label="output")
            # plt.legend()
    
    if sys.argv[3] != "no":
        plt.show()
    print idx
    


          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
