import numpy as np
import theano
import theano.tensor as TT
import sys, os
import pickle

def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])
class RNN_BPTT():
    def __init__(self, nh, ni, no, nb):
        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # number of parametric biases
        self.nb = nb

        # input (where first dimension is time)
        self.u = TT.matrix()
        # parametric bias (where first dimension is time)
        self.pb = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()


        # parametric bias value change
        self.Dpb = theano.shared(np.zeros((self.nb)).astype(theano.config.floatX))
        self.pb_v_updt = theano.shared(np.zeros((self.nb)).astype(theano.config.floatX))
        self.pb_v = theano.shared(np.random.uniform(size=(self.nb), low=-.001, high=.001).astype(theano.config.floatX))
        # self.pb_v = theano.shared(np.ones((self.nb)).astype(theano.config.floatX)+6)
        self.pb_v = TT.set_subtensor(self.pb_v[-1], 6)

        #output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.W = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_pb = theano.shared(np.random.uniform(size=(self.nb, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin, self.n)).astype(theano.config.floatX))
        # bias to hidden layer momentum
        self.mW_pb = theano.shared(np.zeros((self.nb, self.n)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout)).astype(theano.config.floatX))

        ##USED IN THE BATCH NET UPDATE
        # recurrent weights as a shared variable
        self.gW = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.gW_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.gW_pb = theano.shared(np.random.uniform(size=(self.nb, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
    
    def _inner_step(self,pbv=False):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, pb_t, h_tm1, W, W_in, W_pb, W_out, W_outb,outb):
            # h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.nnet.sigmoid(TT.dot(pb_t, W_pb)) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            # y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out)) + TT.nnet.sigmoid(TT.dot(outb, W_outb))
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(pb_t, W_pb) + TT.dot(h_tm1, W))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        if pbv:
            def step_pbv(u_t, h_tm1, pb_v_tm1, W, W_in, W_pb, W_out, W_outb,outb):
                # h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.nnet.sigmoid(TT.dot(pb_t, W_pb)) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
                # y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out)) + TT.nnet.sigmoid(TT.dot(outb, W_outb))
                h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(pb_v_tm1, W_pb) + TT.dot(h_tm1, W))
                y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb))
                return h_t, y_t

            [h, y], _ = theano.scan(step_pbv,
                                sequences=[self.u],
                                outputs_info=[self.h0, None],
                                non_sequences=[self.pb_v, self.W, self.W_in, self.W_pb, self.W_out, self.W_outb, self.outb])
            return h,y

        else:
            [h, y], _ = theano.scan(step,
                                sequences=[self.u, self.pb],
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_pb, self.W_out, self.W_outb, self.outb])
        return h,y
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        h,y = self._inner_step()

        # error between output and target
        error = ((y - self.t) ** 2).sum()

        # gradients on the weights using BPTT
        gW, gW_in, gW_pb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_pb, self.W_out, self.W_outb])

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.h0, self.u, self.pb, self.t, self.lr, self.mom],
                        [error,y],
                        updates={(self.mW, self.mom*self.mW - self.lr * gW),
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_pb, self.mom*self.mW_pb - self.lr * gW_pb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.W, self.W + self.mW),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_pb, self.W_pb + self.mW_pb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_out, self.W_out + self.mW_out)}
                        )

    def _net_step(self,pbv=True):
        # make the inner step
        h,y = self._inner_step(pbv=True)
        
        # calulate error
        error = ((y - self.t) ** 2).sum()

        # calculate gradient only for the pb
        gW, gW_in, gW_pb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_pb, self.W_out, self.W_outb])

        grad_sum = 0.6*TT.sum(gW_pb,1)
        yaw = theano.printing.Print('this is a very important value')(grad_sum)
        yaww = theano.printing.Print('this is the very important value')(self.pb_v_updt)
        # self.pb_v_updt =  0.9*self.pb_v_updt + 0.1*grad_sum
        
        self.pb_v = TT.set_subtensor(self.pb_v[:-1], self.pb_v[:-1] + self.pb_v_updt[:-1])
        
        self.pb_v = TT.nnet.sigmoid(self.pb_v)
        # compile and return the function
        return theano.function(inputs=[self.h0, self.u, self.t],
                        outputs=[error, y, h, self.pb_v, grad_sum, self.pb_v_updt],
                        updates={
                            (self.pb_v_updt, 0.5*self.pb_v_updt + 0.5*grad_sum)
                        })

    def _load_weights(self,):
        import pickle
        f = open("net_trained_new_right_no_monentum.pickle","rb")

        net_w = pickle.load(f)
        
        self.W.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_in.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_pb.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[3].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[4].get_value()))

        f.close()
def net_train(net, dtsets, repetathions):

    for targets in dtsets[:-1]:
        
        targets = _normalise(targets)

        
        
        target_out = []
        for t in targets:
            target_out.append(
                np.append(t[1:],t[0]).reshape(t.shape)
                )
        
        for _ in xrange(repetathions):
                
            print " Rep ", _,
            for idx, stuff in enumerate(zip(targets, target_out)):
                i, o =  stuff
                # print i[0].shape
                pb = np.zeros(len(targets)+1)
                pb[-1] = 6.
                pb[idx] = 6.
                what = net_trainer_accumulator(
                    np.zeros(hiden_size).astype(theano.config.floatX), 
                    i, 
                    np.tile(pb, 
                        o.shape[0]
                        ).reshape(o.shape[0], len(targets)+1),
                    o, 
                    0.0005,
                    0.5
                    )
                
                print "\tMSE ", what[0], pb, #"\n output ", " ".join([str("\n "+str(x)+" | "+str(y)) for x,y in zip(what[1],o)]), #" \r",    
            print " "

def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array):
    
    f = open("net_trained_3_hidden.pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()

def _normalise(targets):
    print "Normalising values ", len(targets), len(targets[0])
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss
    return targets

if __name__ == "__main__":
    
    if len(sys.argv) < 3:
        print "Usage "+sys.argv[0]+" [dirname] [extension]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])

    out_size = 6
    in_size = 6
    pb_size = len(dtsets[0])+1
    hiden_size = 3

    repetathions = 50000

    net = RNN_BPTT(hiden_size, in_size, out_size, pb_size)
    # net._load_weights()
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    # from theano import pp
    # print pp(net_trainer_stepper.maker.fgraph.outputs[0])
    
    print "NET CONF ",hiden_size, in_size, out_size, pb_size

    net_train(net, dtsets, repetathions)
            
    
    import matplotlib.pyplot as plt
    
    targets = _normalise(dtsets[-1])
    target_out = []
    for t in targets:
        target_out.append(
            np.append(t[1:],t[0]).reshape(t.shape)
            )
    
    save_weights([net.W, net.W_in, net.W_pb, net.W_out, net.W_outb])

    
    
    succes_rate=np.zeros(len(targets))
    h = np.zeros(hiden_size).astype(theano.config.floatX)
    for idx, stuff in enumerate(zip(targets, target_out)):
        
        i, o =  stuff
        
        step = 20
        value_change = np.zeros(len(targets)+1)
        
        print "~~~~~~~~~~~~~~"
        pbz_plot = []
        pbs_plot = []
        out_plot = []
        
        net_in_closed_loop = i[0:step]
        net_out_closed_loop = o[0:step]
        the_other_one = -1
        for index, al in enumerate(zip(i,o)):
            
            if index > (len(o)-step):
                break
            for __ in xrange(100):
                what = net_trainer_stepper(
                    h,
                    net_in_closed_loop,
                    net_out_closed_loop 
                    )
            
                error, y, h, pb, grad_sum, pb_updt = what
                
                # next cycle inputs
                h = h[-1]
                net_in_closed_loop = .5*i[index:index+step] + .5*y
                net_out_closed_loop = o[index:index+step]


            
            # print pb.shape
            # pb = 1/(1 + np.power(np.e, -pb))
            print "PB ", pb
            print "Grad Sum ", grad_sum
            print "Pb Updt ", pb_updt
            print "error ", error
            # print "yaw ", yaw
            # print " y ", y.shape
            # grad_W_pb = np.sum(grad_W_pb,1)
            
            # print "grad shape ", grad_W_pb

            #np.append(net_in_closed_loop,y[0])
            # net_in_closed_loop = net_in_closed_loop[1:]
            print index, index+step
            #np.append(net_out_closed_loop,al[1])
            # net_out_closed_loop = net_out_closed_loop[1:]

            # pbz_plot.append(grad_W_pb[:-1])
            pbs_plot.append(1/(1 + np.power(np.e, -pb[:-1])))
            out_plot.append(y[0])

            # if len(pbz_plot) > 51:
                
            #     pb_buffer = pbz_plot[-50:]

            #     pb1 = .5 * (np.sum(pb_buffer,0))
                
            #     pb2 = .0 * (pb_buffer[-3] - 2*pb_buffer[-2] + pb_buffer[-1])
                
            #     value_change = 0.1 * (pb1+pb2) + 0.9 * value_change
                
            #     pb[:][:-1] += value_change[:-1]
            #     # pb[:-1] = 1/ (1 + np.power(np.e, -pb[:-1]) )

            #     # print "pb1", pb1,    
            #     # print "pb2", pb2
            #     # print "value change ", value_change[:-1]
            #     # print net_in_closed_loop
            #     # print net_out_closed_loop
            #     print "error ", error,
            #     print ""+ str(idx) +" parametric bias ", np.argmax(pb[:][:-1]),
            #     print "\n ", pb
            #     print "\ngradient ", grad_W_pb 
            #     # print "\r",
            # else:
            #     print "not yet.."

            if idx == np.argmax(pb[:-1]):
                succes_rate[idx]+=1

        print succes_rate, " of ", [len(t) for t in targets]

    #     plt.figure()
    #     plt.ylabel('Values Seq'+str(idx))
    #     plt.xlabel('Time')
    #     plt.plot(pbs_plot[-len(o):],"--",label="Pb")
    #     # plt.plot(o,"-")
    #     plt.legend()
    #     # plt.figure()
    #     # plt.plot(pbz_plot,"--", label="gradient")
    #     # plt.figure()
    #     plt.plot(out_plot,"-", label="output")
    #     plt.legend()
    
    # plt.show()
    print idx
    


          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
# if the_one != the_other_one:
#     the_other_one = the_one
#     pb = np.random.uniform(size=len(targets)+1, low=-0.001, high=0.001)
# np.tile(
#     net_in_closed_loop,step).reshape(
#     step,net_in_closed_loop.shape[0]), 
# np.tile(
#     pb,step).reshape(
#     step,pb.shape[0]),
# np.tile(
#     net_out_closed_loop,step).reshape(
#     step,net_out_closed_loop.shape[0])
