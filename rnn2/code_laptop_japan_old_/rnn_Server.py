import numpy as np
import theano
import theano.tensor as TT


def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])
class RNN_BPTT():
    def __init__(self, nh, ni, no, nb):
        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # number of parametric biases
        self.nb = nb

        # input (where first dimension is time)
        self.u = TT.matrix()
        # parametric bias (where first dimension is time)
        self.pb = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()


        # parametric bias value change
        self.Dpb = theano.shared(np.zeros((self.nb)).astype(theano.config.floatX))

        #output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.W = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_pb = theano.shared(np.random.uniform(size=(self.nb, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin, self.n)).astype(theano.config.floatX))
        # bias to hidden layer momentum
        self.mW_pb = theano.shared(np.zeros((self.nb, self.n)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout)).astype(theano.config.floatX))

        ##USED IN THE BATCH NET UPDATE
        # recurrent weights as a shared variable
        self.gW = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.gW_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.gW_pb = theano.shared(np.random.uniform(size=(self.nb, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, pb_t, h_tm1, W, W_in, W_pb, W_out, W_outb,outb):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.nnet.sigmoid(TT.dot(pb_t, W_pb)) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out)) + TT.nnet.sigmoid(TT.dot(outb, W_outb))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=[self.u, self.pb],
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_pb, self.W_out, self.W_outb, self.outb])
        return h,y
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        h,y = self._inner_step()

        # error between output and target
        error = ((y - self.t) ** 2).sum()

        # gradients on the weights using BPTT
        gW, gW_in, gW_pb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_pb, self.W_out, self.W_outb])

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.h0, self.u, self.pb, self.t, self.lr, self.mom],
                        [error,y],
                        updates={(self.mW, self.mom*self.mW - self.lr * gW),
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_pb, self.mom*self.mW_pb - self.lr * gW_pb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.W, self.W + self.mW),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_pb, self.W_pb + self.mW_pb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_out, self.W_out + self.mW_out)}
                        )

    def _net_step(self,):
        # make the inner step
        h,y = self._inner_step()
        
        # calulate error
        error = ((y - self.t) ** 2).sum()

        # calculate gradient only for the pb
        gW, gW_in, gW_pb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_pb, self.W_out, self.W_outb])

        # compile and return the function
        return theano.function([self.h0, self.u, self.pb, self.t],
                        [error, y, h, self.pb, gW_pb])

    def _load_weights(self,):
        import pickle
        f = open("net_trained.pickle","rb")

        net_w = pickle.load(f)
        
        self.W.set_value(net_w[0].get_value())
        self.W_in.set_value(net_w[1].get_value())
        self.W_pb.set_value(net_w[2].get_value())
        self.W_out.set_value(net_w[3].get_value())
        self.W_outb.set_value(net_w[4].get_value())

        f.close()

if __name__ == "__main__":
    
    import pickle

    f = open("6D_moves.pickle", "rb")
    targets = pickle.load(f)
    targets = targets[:2]
    
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss

    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)


    out_size = 6
    in_size = 6
    pb_size = len(targets)+1
    hiden_size = 20

    repetathions = 50000

    net = RNN_BPTT(hiden_size, in_size, out_size, pb_size)
    # net._load_weights()
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    
    target_out = []
    for t in targets:
        target_out.append(
            np.append(t[1:],t[0]).reshape(t.shape)
            )
    plot_out=[]
    for _ in xrange(repetathions):
            
        print " Rep ", _,
        for idx, stuff in enumerate(zip(targets, target_out)):
            i, o =  stuff
            # print i[0].shape
            pb = np.zeros(len(targets)+1)
            pb[-1] = 6.
            pb[idx] = 6.
            what = net_trainer_accumulator(
                np.zeros(hiden_size).astype(theano.config.floatX), 
                i, 
                np.tile(pb, 
                    o.shape[0]
                    ).reshape(o.shape[0], len(targets)+1),
                o, 
                0.021,
                0.0
                )
            
            print "\tMSE ", what[0], pb, #"\n output ", " ".join([str("\n "+str(x)+" | "+str(y)) for x,y in zip(what[1],o)]), #" \r",    
        print " "
        
        if _ > 1:
            import matplotlib.pyplot as plt
            '''
            import pickle
            f = open("net_trained.pickle", "wb")
            
            ar = np.array([net.W, net.W_in, net.W_pb, net.W_out, net.W_outb])
            pickle.dump(ar, f)
            f.close()
            '''
            # plt.figure()
            
            for idx, stuff in enumerate(zip(targets, target_out)):
                
                i, o =  stuff
                h = np.zeros(hiden_size).astype(theano.config.floatX)
                pbz_plot = []
                pbs_plot = []
                out_plot = []
                step = 5
                pb = np.random.uniform(size=len(targets)+1, low=-0.01, high=0.01)
                pb[-1] = 6.
                value_change = 0
                print "~~~~~~~~~~~~~~"
                for index, al in enumerate(zip(i,o)):
                    for __ in xrange(step):
                        what = net_trainer_stepper(
                            h, 
                            [al[0]], 
                            [pb],
                            [al[1]]
                            )
                        
                        error, y, h, pb, grad_W_pb = what
                        h = h[0]
                        pb = pb[0]
                        grad_W_pb = np.sum(grad_W_pb,1)
                        pbz_plot.append(grad_W_pb)
                        pbs_plot.append(pb)
                        out_plot.append(y[0])

                        if len(pbz_plot) > 33:
                            pb_buffer = pbz_plot[-30:]
                            pb1 = np.sum(pb_buffer,0)/30
                            
                            pb2 = pb_buffer[-3] - 2*pb_buffer[-2] + pb_buffer[-1]
                            
                            print "pb1", pb1,    
                            print "\tpb2", pb2
                            value_change = 0.9*(pb1+pb2) + 0.2*value_change
                            
                            pb[:-1] += value_change[:-1]
                            # pb[:-1] = 1/(1 + np.power(np.e, -pb[:-1]))

                            print "value change ", value_change[:-1]
                            print "gradient ", grad_W_pb , 
                            print "\terror ", error
                            print "parametric bias ", pb[:-1], np.argmax(pb[:-1])
                        else:
                            print "not yet.."
                plt.figure()
                plt.plot(pbs_plot[::step],"--", label="parametric bias")
                plt.plot(o,"-", label="target")
                plt.legend()
                plt.figure()
                plt.plot(out_plot,"-", label="output")
                plt.legend()
            plt.show()
            print idx
            break


          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
