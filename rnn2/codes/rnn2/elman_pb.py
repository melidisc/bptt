import numpy as np
import theano
import theano.tensor as TT
import sys, os, pickle


class RNN_BPTT():
    def __init__(self, nh, ni, no, nb, bs, in_dtst, pb_dtst, out_dtst):

        self.batch_size = bs
        
        self.in_dtst = theano.shared(in_dtst)
        self.pb_dtst = theano.shared(pb_dtst)
        self.out_dtst = theano.shared(out_dtst)
        
        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # number of parametric biases
        self.nb = nb

        # input (where first dimension is time)
        self.u = TT.matrix()
        # parametric bias (where first dimension is time)
        self.pb = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()

        self.index = TT.lscalar()


        # parametric bias value change
        self.Dpb = theano.shared(np.zeros((self.nb)).astype(theano.config.floatX))

        #output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))
        scale = 0.0001
        # recurrent weights as a shared variable
        self.W = theano.shared(scale*np.random.uniform(size=(self.n, self.n), low=-1, high=1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(scale*np.random.uniform(size=(self.nin, self.n), low=-1, high=1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_pb = theano.shared(scale*np.random.uniform(size=(self.nb, self.n), low=-1, high=1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(scale*np.random.uniform(size=(self.n, self.nout), low=-1, high=1).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(scale*np.random.uniform(size=(1, self.nout), low=-1, high=1).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin, self.n)).astype(theano.config.floatX))
        # bias to hidden layer momentum
        self.mW_pb = theano.shared(np.zeros((self.nb, self.n)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout)).astype(theano.config.floatX))

        ##USED IN THE BATCH NET UPDATE
        # recurrent weights as a shared variable
        self.gW = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.gW_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.gW_pb = theano.shared(np.random.uniform(size=(self.nb, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, pb_t, h_tm1, W, W_in, W_pb, W_out, W_outb,outb):
            # h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.nnet.sigmoid(TT.dot(pb_t, W_pb)) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            # y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out)) + TT.nnet.sigmoid(TT.dot(outb, W_outb))
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(pb_t, W_pb) + TT.dot(h_tm1, W))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=[self.u, self.pb],
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_pb, self.W_out, self.W_outb, self.outb])
        return h,y
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        h,y = self._inner_step()

        # error between output and target
        error = ((y - self.t) ** 2).mean()

        # gradients on the weights using BPTT
        gW, gW_in, gW_pb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_pb, self.W_out, self.W_outb])

        # training function, that computes the error and updates the weights using
        # SGD.

        return theano.function([self.h0, self.index, self.lr, self.mom],
                        [error,y],
                        updates={(self.mW, self.mom*self.mW - self.lr * gW),
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_pb, self.mom*self.mW_pb - self.lr * gW_pb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.W, self.W + self.mW),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_pb, self.W_pb + self.mW_pb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_out, self.W_out + self.mW_out)},
                        givens={(self.u, self.in_dtst[self.index * self.batch_size: (self.index + 1) * self.batch_size]),
                                (self.pb, self.pb_dtst[self.index * self.batch_size: (self.index + 1) * self.batch_size]),
                                (self.t, self.out_dtst[self.index * self.batch_size: (self.index + 1) * self.batch_size])}

                        )

    def _net_step(self,):
        # make the inner step
        h,y = self._inner_step()
        
        # calulate error
        error = ((y - self.t) ** 2).sum()

        # calculate gradient only for the pb
        gW, gW_in, gW_pb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_pb, self.W_out, self.W_outb])

        # compile and return the function
        return theano.function([self.h0, self.u, self.pb, self.t],
                        [error, y, h, self.pb, gW_pb])

    def _load_weights(self,filename):
        import pickle
        folder = "trained_elman_pb/"

        f = open(folder+filename+".pickle","rb")

        net_w = pickle.load(f)
        
        self.W.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_in.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_pb.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[3].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[4].get_value()))

        f.close()