import numpy as np
import theano
import theano.tensor as TT
import sys, os
import pickle
from jordan_pb_tani import RNN_TANI_BPTT as RNN_BPTT

def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])
def make_train_set(dataset, outc_size, pb_size):
    '''
        returns 3 lists
        inputs: all the input series from all the movement sets, as ones
        bias: the paramteric bias trainig values
        outputs: creates the output targets for the input listss
    '''
    inputs = []
    bias = []
    outputs = []
    gestures_length = [0]
   
    for a_trial in dataset:
        a_trial = _normalise(a_trial)
        for idx, gesture in enumerate(a_trial):
            gestures_length.append(len(gesture)+gestures_length[-1])
            
            tmp_cls = np.zeros(pb_size + outc_size)
            tmp_cls[idx] = 1.

            _in = np.array([np.append(x, tmp_cls) for x in gesture])
            inputs.extend(_in.astype(theano.config.floatX))

            tmp_out = np.append(gesture[1:],gesture[0]).reshape(gesture.shape).astype(theano.config.floatX)

            tmp_cls = np.zeros(outc_size)
            _out = [np.append(x, tmp_cls) for x in tmp_out]
            outputs.extend(
                _out
                )
            
            # pb = np.zeros(len(a_trial))
            # pb[idx] = 1.
            
            # parametric_bias = np.tile(pb, 
            #         gesture.shape[0]
            #         ).reshape(gesture.shape[0], len(a_trial)).astype(theano.config.floatX)
            # bias.extend(parametric_bias)

            #, np.array(bias)

    print gestures_length
    return np.array(inputs), np.array(outputs), gestures_length
def net_train(net_trainer_accumulator, repetathions, lr, mr, idxs, pb_vs):
    series = []
    for idx in range(1, len(idxs)):
        series.append( (idxs[idx-1],idxs[idx]))
    pbs = np.random.random(pb_vs)
    for _ in xrange(repetathions):
        error_acc = 0
        for i, s in enumerate(series):
            # print pbs[i%pb_vs[0]]
            out_class = np.zeros(outc_size).astype(theano.config.floatX)
            what = net_trainer_accumulator(
                out_class , 
                # pbs[i%pb_vs[0]],
                s[0],#from
                s[1],#to
                lr, 
                mr
                )
            error_acc += what[0]
            out_class = what[2]
            pbs[i%pb_vs[0]] = what[3]
            print pbs[i%pb_vs[0]]
        print str(_) +"\t\t\t\t MSE ", error_acc #number of sequences
    print ""    
    # for idx in xrange(batch_number):
    #     what = net_trainer_accumulator(
    #         out_class , 
    #         idx,
    #         lr, 
    #         mr
    #         )
    #     error_acc += what[0]
    #     out_class = what[2]

def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array, filename):
    
    folder = "trained_elman_out_rec/"

    f = open(folder+"net_tani_trained_"+filename+".pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()
def _normalise(targets):
    print "Normalising values ", len(targets), len(targets[0])
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss
    return targets

if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print "Usage "+sys.argv[0]+" [dirname] [extension] [file _load_weights] [file save_weights]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])

    out_size = 6
    in_size = 6
    outc_size = 10
    pb_size = 4#len(dtsets[0])
    hiden_size = 20
    batch_size = 40
    lr = 0.21
    mr = 0.3


    repetathions = 1000

    all_inputs, all_outputs, idxs = make_train_set(dtsets[:-1], outc_size, pb_size)
    print all_inputs[-1].shape,  all_outputs[-2].shape

    net = RNN_BPTT(
        hiden_size, 
        in_size, 
        out_size, 
        outc_size, 
        pb_size,
        batch_size, 
        all_inputs.astype(theano.config.floatX), 
        all_outputs.astype(theano.config.floatX)
        )
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    print "NET CONF ",hiden_size, in_size, out_size, outc_size, lr, mr

    if sys.argv[3] == "no":
        print "Training"
        # batch_number = all_inputs.shape[0] / batch_size
        net_train(net_trainer_accumulator, repetathions,lr, mr,idxs, ( len(dtsets[0]),pb_size ))
    else:
        print "Whaaat"
        net._load_weights(sys.argv[3])
        import matplotlib.pyplot as plt
    
    # targets = _normalise(dtsets[-1])
    # target_out = []
    # for t in targets:
    #     target_out.append(
    #         np.append(t[1:],t[0]).reshape(t.shape)
    #         )

    if sys.argv[4] != "no":
        save_weights(
            [net.W_in, 
            net.W_inb, 
            net.W_out, 
            net.W_outb],  
            sys.argv[4]
            )

    


    succes_rate=np.zeros(pb_size)
    error_rate=np.zeros(pb_size)
    
    error_plot = []
    pbs_plot = []
    out_plot = []
    
    h = np.zeros(hiden_size).astype(theano.config.floatX)
    outc = np.zeros(outc_size).astype(theano.config.floatX)
    what_else=[]

    series = []
    for idx in range(1, len(idxs)):
        series.append( (idxs[idx-1],idxs[idx]))
    
    #test on training set
    for s in series:
        outc = np.zeros(outc_size).astype(theano.config.floatX)
        inp = all_inputs[s[0]: s[1]]
        what = net_trainer_stepper(outc,inp)
        pbv, y, er, outc = what
        print pbv
        error_plot.extend(np.tile(er,batch_size))
        out_plot.extend(y)
        pbs_plot.append(pbv)
    
    if sys.argv[3] != "no":
        plt.figure()
        plt.plot(out_plot,label="net output")
        plt.plot(all_outputs,"--",label="real output")
        plt.figure()
        plt.plot(error_plot,"--",label="error output")
        plt.plot(pbs_plot,"o",label="pb values")
        plt.legend()
        plt.show()
    
    print "IN TRAINING SET, succes_rate",succes_rate,"error_rate", error_rate
    outc = np.zeros(outc_size).astype(theano.config.floatX)
    succes_rate=np.zeros(pb_size)
    error_rate=np.zeros(pb_size)
    

    targets, targets_out, idxs = make_train_set([dtsets[-1]], outc_size, pb_size)
    class_plot = []    
    out_plot = []
    #real test
    series = []
    for idx in range(1, len(idxs)):
        series.append( (idxs[idx-1],idxs[idx]))

    for s in series:
        inp = targets[s[0] : s[1]]
        print "Series "+str(s[0])+" to "+str(s[1])  
        # for __ in xrange(10):
        what = net_trainer_stepper(outc,inp)
            # outc = what[-1]
        pbv, y, er, outc = what
        print pbv
        out_plot.extend(y)
        class_plot.append(pbv)
    
    if sys.argv[3] != "no":
        plt.figure()
        plt.plot(out_plot,"--",label="net output")
        plt.legend()
        plt.figure()
        plt.plot(targets_out,label="real output")
        plt.legend()
        plt.figure()
        plt.plot(class_plot,label="class values")
        plt.legend()
        plt.show()

    class_plot = []    
    out_plot = []
    
    test_bsize = 15
    for idx in xrange(len(targets)-test_bsize):
        inp = targets[idx : idx + test_bsize]

        for __ in xrange(10):
            what = net_trainer_stepper(outc,inp)
            pbv, y, er, outc = what

        pbv, y, er, outc = what
        # print pbv
        out_plot.extend(y)
        class_plot.append(pbv)

    if sys.argv[3] != "no":
        plt.figure()
        plt.plot(out_plot,"--",label="net output")
        plt.legend()
        plt.figure()
        plt.plot(targets_out,label="real output")
        plt.legend()
        plt.figure()
        plt.plot(class_plot,label="class values")
        plt.legend()
        plt.show()
    print "IN TEST SET, succes_rate",succes_rate,"error_rate", error_rate
    


          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
'''
    for _ in xrange(repetathions):
        
        for targets in dtsets[:-1]:
            
            targets = _normalise(targets)

            target_out = []
            for t in targets:
                target_out.append(
                    np.append(t[1:],t[0]).reshape(t.shape)
                    )
            
                
            print " Rep ", _,
            

            ov_er = 0
            for idx, stuff in enumerate(zip(targets, target_out)):
                h = np.zeros(hiden_size).astype(theano.config.floatX)
                outc = np.zeros(outc_size).astype(theano.config.floatX)
                i, o =  stuff

                pb = np.zeros(len(targets))
                pb[idx] = 1

                oo = []
                for outt in o:
                    oo.append(
                        np.append(outt, pb)
                        )
                o = np.array(oo)
                ii = []
                for inn in i:
                    ii.append(
                        np.append(inn, np.zeros(len(targets)))
                        )
                i = np.array(ii)
                # pb = np.tile(pb, i.shape[0]).reshape(i.shape[0], len(targets))

                what = net_trainer_accumulator(
                    h.astype(theano.config.floatX),
                    outc.astype(theano.config.floatX), 
                    i.astype(theano.config.floatX), 
                    o.astype(theano.config.floatX),
                    0.00081,
                    0.0
                    )
                outc = what[-2]
                h = what[-1]
                # print "\nMSE _",idx,"_", what[0], " \n", outc, pb
                ov_er+= what[0]

            print "Overal error ", ov_er
    '''