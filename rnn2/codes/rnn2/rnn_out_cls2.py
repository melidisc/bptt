import numpy as np
import theano
import theano.tensor as TT
import sys, os
import pickle
from elman_out_rec import RNN_BPTT_2 as RNN_BPTT

def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])
def net_train(net, dtsets, repetathions):

    for _ in xrange(repetathions):
        
        for targets in dtsets[:-1]:
            
            targets = _normalise(targets)

            target_out = []
            for t in targets:
                target_out.append(
                    np.append(t[1:],t[0]).reshape(t.shape)
                    )
            
                
            print " Rep ", _,
            

            ov_er = 0
            for idx, stuff in enumerate(zip(targets, target_out)):
                h = np.zeros(hiden_size).astype(theano.config.floatX)
                outc = np.zeros(outc_size).astype(theano.config.floatX)
                i, o =  stuff

                pb = np.zeros(len(targets))
                pb[idx] = 1

                oo = []
                for outt in o:
                    oo.append(
                        np.append(outt, pb)
                        )
                o = np.array(oo)
                ii = []
                for inn in i:
                    ii.append(
                        np.append(inn, np.zeros(len(targets)))
                        )
                i = np.array(ii)
                # pb = np.tile(pb, i.shape[0]).reshape(i.shape[0], len(targets))

                what = net_trainer_accumulator(
                    h.astype(theano.config.floatX),
                    outc.astype(theano.config.floatX), 
                    i.astype(theano.config.floatX), 
                    o.astype(theano.config.floatX),
                    0.00081,
                    0.0
                    )
                outc = what[-2]
                h = what[-1]
                # print "\nMSE _",idx,"_", what[0], " \n", outc, pb
                ov_er+= what[0]

            print "Overal error ", ov_er
            

def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array, filename):
    
    folder = "trained_elman_out_rec/"

    f = open(folder+"net_cls2_trained_"+filename+".pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()
def _normalise(targets):
    print "Normalising values ", len(targets), len(targets[0])
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss
    return targets

if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print "Usage "+sys.argv[0]+" [dirname] [extension] [file _load_weights] [file save_weights]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])

    out_size = 6
    in_size = 6
    outc_size = len(dtsets[0])
    hiden_size = 11

    repetathions = 5000

    net = RNN_BPTT(hiden_size, in_size, out_size, outc_size)
    if sys.argv[3] != "no":
        net._load_weights(sys.argv[3])
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    print "NET CONF ",hiden_size, in_size, out_size, outc_size

    if sys.argv[3] == "no":
        print "Training"
        net_train(net, dtsets, repetathions)
    else:
        print "Whaaat"
        import matplotlib.pyplot as plt
    
    targets = _normalise(dtsets[-1])
    target_out = []
    for t in targets:
        target_out.append(
            np.append(t[1:],t[0]).reshape(t.shape)
            )

    if sys.argv[4] != "no":
        save_weights(
            [net.W, 
            net.W_in, 
            net.W_inb, 
            net.W_out, 
            net.W_outb],  
            sys.argv[4]
            )

    
    succes_rate=np.zeros(len(targets))
    
    pbz_plot = []
    pbs_plot = []
    out_plot = []
    
    h = np.zeros(hiden_size).astype(theano.config.floatX)
    outc = np.zeros(outc_size).astype(theano.config.floatX)
    
    for idx, stuff in enumerate(zip(targets, target_out)):
        
        i, o =  stuff
        
        ii = []
        for inn in i:
            ii.append(
                np.append(inn, np.zeros(len(targets)))
                )
        i = np.array(ii)

        what = net_trainer_stepper(h,outc,i)
        
        y = what[0]
        
        print "classifier ", y[-1][-outc_size:]

        pbs_plot=y[:][-outc_size:]
        out_plot=y[:][:outc_size]
        # print "error", error
        # print succes_rate, " of ", [len(t) for t in targets]

        if sys.argv[3] != "no":
            plt.figure()
            plt.ylabel('Values Seq'+str(idx))
            plt.xlabel('Time')
            plt.plot(pbs_plot,"--",label="Pb")
            plt.plot(o,"-")
            plt.legend()
            plt.figure()
            plt.plot(out_plot,"-", label="output")
            plt.legend()
    
    if sys.argv[3] != "no":
        plt.show()
    print idx
    


          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
