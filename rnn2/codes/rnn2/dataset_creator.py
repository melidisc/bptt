import numpy as np
import sys
import matplotlib.pyplot as plt

if __name__="__main__":

	if len(sys.argv)<3:
		print "Gimme the [location] and [name] of the dataset to create "
		sys.exit(0)
	#create file 
	f = open(sys.arv[1]+"/"+sys.arv[2]+".pickle","wb")
	#create the dataset
	noisy = True
	g_number = 5
	g_length = 500 
	init_size = 33
	reps = 3
	dtst = []
	for j in xrange(reps):
		trial = []
		for i in xrange(g_number):
			tile_size = (g_length / init_size) + int(np.random.random(1) * 10)
			a = 0
			if noisy:
				a = np.random.random(init_size)
			a_linspace = np.linspace(0, 150*(2*np.pi), init_size) + a

			coin = np.random.random(1)
			if coin > .5:
				gesture = np.sin(np.tile(a_linspace,tile_size))
			else:
				gesture = np.cos(np.tile(a_linspace,tile_size))
			trial.append(gesture)
		dtst.append(trial)
	
	for t in dtst:
		for g in t:
			pass
	plt.figure()
	plt.plot(g)

	#store 

	#bye!