import numpy as np
import theano
import theano.tensor as TT
import theano.sandbox.cuda.basic_ops
import sys, os
import pickle

class RNN_BPTT():
    def __init__(self, nh, ni, no, noc):
        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # numbe of reccuring ouputs
        self.noc = noc
        
        # input (where first dimension is time)
        self.u = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # target (where first dimension is time)
        self.outc = TT.matrix()
        
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # initial hidden state of the RNN
        self.outc0 = TT.vector()
        
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()

        # input bias
        self.inb = theano.shared(np.ones((1)).astype(theano.config.floatX))
        # output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.W = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # recurrent output weights as a shared variable
        self.W_outc_h = theano.shared(np.random.uniform(size=(self.noc, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # recurrent output weights as a shared variable
        self.W_h_outc = theano.shared(np.random.uniform(size=(self.n, self.noc), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_inb = theano.shared(np.random.uniform(size=(1, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        self.W_outcb = theano.shared(np.random.uniform(size=(1, self.noc), low=-.1, high=.1).astype(theano.config.floatX))

        # recurrent moemntum weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin, self.n)).astype(theano.config.floatX))
        # input bias momentum
        self.mW_inb = theano.shared(np.zeros((1, self.n)).astype(theano.config.floatX))
        # in class to hidder momentum
        self.mW_outc_h = theano.shared(np.zeros((self.noc, self.n)).astype(theano.config.floatX))
        # hidden to out class
        self.mW_h_outc = theano.shared(np.zeros((self.n, self.noc)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout)).astype(theano.config.floatX))
        self.mW_outcb = theano.shared(np.zeros((1, self.noc)).astype(theano.config.floatX))

        ##USED IN THE BATCH NET UPDATE
        # recurrent weights as a shared variable
        self.gW = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.gW_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, outc_tm1, W, W_in, W_pb, W_out, W_outb, W_outcb, W_h_outc, W_outc_h, outb, inb):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(inb, W_pb) + TT.dot(h_tm1, W) + TT.dot(outc_tm1, W_outc_h))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb) )
            outc_t = TT.nnet.sigmoid(TT.dot(h_t, W_h_outc) + TT.dot(outb, W_outcb))
            return h_t, y_t, outc_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y, outc], _ = theano.scan(step,
                                sequences=[self.u],
                                outputs_info=[self.h0, None, self.outc0],
                                non_sequences=[self.W, self.W_in, self.W_inb, self.W_out, self.W_outb, self.W_outcb, self.W_h_outc, self.W_outc_h, self.outb, self.inb])
        return h,y,outc
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        h, y, outc = self._inner_step()
        
        # error between output and target
        # error_1 = TT.nnet.categorical_crossentropy(y, self.t).sum()
        error_1 = ((y - self.t) ** 2).sum()
        error_2 = TT.nnet.categorical_crossentropy(outc, self.outc).sum()
        # error_2 = ((outc - self.outc) ** 2).sum()
        
        error = error_1 + error_2

        # gradients on the weights using BPTT
        gW, gW_in, gW_inb, gW_out, gW_outb, gW_outcb, gW_h_outc, gW_outc_h = TT.grad(error, [self.W, self.W_in, self.W_inb, self.W_out, self.W_outb, self.W_outcb, self.W_h_outc, self.W_outc_h])

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.h0, self.outc0, self.u, self.t, self.outc, self.lr, self.mom],
                        [error_1, error_2, y, outc[-1], gW_h_outc, gW_outc_h],
                        updates={(self.mW, self.mom*self.mW - self.lr * gW),
                                (self.mW_outc_h, self.mom*self.mW_outc_h - self.lr * gW_outc_h),
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_inb, self.mom*self.mW_inb - self.lr * gW_inb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_h_outc, self.mom*self.mW_h_outc - self.lr * gW_h_outc),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.mW_outcb, self.mom*self.mW_outcb - self.lr * gW_outcb),
                                (self.W, self.W + self.mW),
                                (self.W_outc_h, self.W_outc_h + self.mW_outc_h),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_inb, self.W_inb + self.mW_inb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_outcb, self.W_outcb + self.mW_outcb),
                                (self.W_out, self.W_out + self.mW_out),
                                (self.W_h_outc, self.W_h_outc + self.mW_h_outc)}
                        )

    def _net_step(self,):
        # make the inner step
        h,y,outc = self._inner_step()
        
        # # calulate error
        # error = ((y - self.t) ** 2).sum()

        # compile and return the function
        return theano.function(inputs=[self.h0, self.outc0, self.u],
                        outputs=[y, outc[0]])

    def _load_weights(self, filename):
        import pickle
        
        folder = "trained_elman_out_rec/"

        f = open(folder+filename+".pickle","rb")

        net_w = pickle.load(f)
    
        self.W.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_h_outc.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_in.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_inb.set_value(
            np.asarray(net_w[3].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[4].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[5].get_value()))
        self.W_outc_h.set_value(
            np.asarray(net_w[6].get_value()))


        f.close()


class RNN_BPTT_2():
    def __init__(self, nh, ni, no, noc):
        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # numbe of reccuring ouputs
        self.noc = noc
        
        # input (where first dimension is time)
        self.u = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # target (where first dimension is time)
        self.outc = TT.matrix()
        
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # initial hidden state of the RNN
        self.outc0 = TT.vector()
        
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()

        
        # input bias
        self.inb = theano.shared(np.ones((1)).astype(theano.config.floatX))
        # output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))

        # recurrent weights as a shared variable
        self.W = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.00001, high=.00001).astype(theano.config.floatX))
        # recurrent output weights as a shared variable
        # self.W_outc_h = theano.shared(np.random.uniform(size=(self.noc, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # recurrent output weights as a shared variable
        # self.W_h_outc = theano.shared(np.random.uniform(size=(self.n, self.noc), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(np.random.uniform(size=(self.nin+self.noc, self.n), low=-.00001, high=.00001).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_inb = theano.shared(np.random.uniform(size=(1, self.n), low=-.00001, high=.00001).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(np.random.uniform(size=(self.n, self.nout+self.noc), low=-.00001, high=.00001).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(np.random.uniform(size=(1, self.nout+self.noc), low=-.00001, high=.00001).astype(theano.config.floatX))
        # self.W_outcb = theano.shared(np.random.uniform(size=(1, self.noc), low=-.1, high=.1).astype(theano.config.floatX))

        # recurrent moemntum weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin+self.noc, self.n)).astype(theano.config.floatX))
        # input bias momentum
        self.mW_inb = theano.shared(np.zeros((1, self.n)).astype(theano.config.floatX))
        # in class to hidder momentum
        # self.mW_outc_h = theano.shared(np.zeros((self.noc, self.n)).astype(theano.config.floatX))
        # hidden to out class
        # self.mW_h_outc = theano.shared(np.zeros((self.n, self.noc)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout+self.noc)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout+self.noc)).astype(theano.config.floatX))
        # self.mW_outcb = theano.shared(np.zeros((1, self.noc)).astype(theano.config.floatX))

        ##USED IN THE BATCH NET UPDATE
        # recurrent weights as a shared variable
        self.gW = theano.shared(np.random.uniform(size=(self.n, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.gW_in = theano.shared(np.random.uniform(size=(self.nin, self.n), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_out = theano.shared(np.random.uniform(size=(self.n, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.gW_outb = theano.shared(np.random.uniform(size=(1, self.nout), low=-.1, high=.1).astype(theano.config.floatX))
    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, outc_tm1, W, W_in, W_pb, W_out, W_outb, outb, inb):
            u_t = TT.set_subtensor(u_t[-self.noc:], outc_tm1)
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(inb, W_pb) + TT.dot(h_tm1, W))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb) )
            outc_t = y_t[-self.noc:]
            return h_t, y_t, outc_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y, outc], _ = theano.scan(step,
                                sequences=[self.u],
                                outputs_info=[self.h0, None, self.outc0],
                                non_sequences=[self.W, self.W_in, self.W_inb, self.W_out, self.W_outb, self.outb, self.inb])
        return h,y,outc
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        h, y, outc = self._inner_step()
        
        # error between output and target
        error = ((y - self.t) ** 2).sum()
        # error = TT.nnet.categorical_crossentropy(y, self.t).sum()

        # gradients on the weights using BPTT
        gW, gW_in, gW_inb, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_inb, self.W_out, self.W_outb])

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.h0, self.outc0, self.u, self.t, self.lr, self.mom],
                        [error, y, outc[-1], h[-1]],
                        updates={(self.mW, self.mom*self.mW - self.lr * gW),
                                # (self.mW_outc_h, self.mom*self.mW_outc_h - self.lr * gW_outc_h),
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_inb, self.mom*self.mW_inb - self.lr * gW_inb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                # (self.mW_h_outc, self.mom*self.mW_h_outc - 100*self.lr * gW_h_outc),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                # (self.mW_outcb, self.mom*self.mW_outcb - self.lr * gW_outcb),
                                (self.W, self.W + self.mW),
                                # (self.W_outc_h, self.W_outc_h + self.mW_outc_h),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_inb, self.W_inb + self.mW_inb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                # (self.W_outcb, self.W_outcb + self.mW_outcb),
                                (self.W_out, self.W_out + self.mW_out)}
                                # (self.W_h_outc, self.W_h_outc + self.mW_h_outc)}
                        )

    def _net_step(self,):
        # make the inner step
        h,y,outc = self._inner_step()
        
        # # calulate error
        # error = ((y - self.t) ** 2).sum()

        # compile and return the function
        return theano.function([self.h0, self.outc0, self.u],
                        [y])

    def _load_weights(self, filename):
        import pickle

        folder = "trained_elman_out_rec/"

        f = open(folder+filename+".pickle","rb")

        net_w = pickle.load(f)
    
        self.W.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_in.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_inb.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[3].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[4].get_value()))


        f.close()

class RNN_BPTT_3():
    def __init__(self, nh, ni, no, noc, bs, in_dtst, out_dtst):

        self.batch_size = bs
        
        self.in_dtst = theano.shared(in_dtst, borrow=True)
        
        self.out_dtst = theano.shared(out_dtst, borrow=True)
        

        self.index = TT.lscalar()
        self.indexto = TT.lscalar()

        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # numbe of reccuring ouputs
        self.noc = noc
        
        # input (where first dimension is time)
        self.u = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # target (where first dimension is time)
        self.outc = TT.matrix()
        
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # initial hidden state of the RNN
        self.outc0 = TT.vector()
        
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()
         # bias adaptation rate
        self.pb_lr = TT.scalar()

        
        # input bias
        self.inb = theano.shared(np.ones((1)).astype(theano.config.floatX), borrow=True)
        # output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX), borrow=True)

        scale = 0.01
        # recurrent weights as a shared variable
        # self.W = theano.shared(scale*np.random.uniform(size=(self.n, self.n), low=-1, high=1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(scale*np.random.uniform(size=(self.nin+self.noc, self.n), low=-1, high=1).astype(theano.config.floatX), borrow=True)
        # bias to hidden layer weights
        self.W_inb = theano.shared(scale*np.random.uniform(size=(1, self.n), low=-1, high=1).astype(theano.config.floatX), borrow=True)
        # hidden to output layer weights
        self.W_out = theano.shared(scale*np.random.uniform(size=(self.n, self.nout+self.noc), low=-1, high=1).astype(theano.config.floatX), borrow=True)
        # hidden to output bias weights
        self.W_outb = theano.shared(scale*np.random.uniform(size=(1, self.nout+self.noc), low=-1, high=1).astype(theano.config.floatX), borrow=True)

        # recurrent moemntum weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX), borrow=True)
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.nin+self.noc, self.n)).astype(theano.config.floatX), borrow=True)
        # input bias momentum
        self.mW_inb = theano.shared(np.zeros((1, self.n)).astype(theano.config.floatX), borrow=True)
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout+self.noc)).astype(theano.config.floatX), borrow=True)
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout+self.noc)).astype(theano.config.floatX), borrow=True)
        # self.mW_outcb = theano.shared(np.zeros((1, self.noc)).astype(theano.config.floatX))

    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, t_t, y_tm1, outc_tm1, W_in, W_inb, W_out, W_outb, outb, inb):#W, 
            
            # u_t = TT.set_subtensor(u_t[-self.noc:], outc_tm1)
            u_t = y_tm1 * 0.5 + TT.set_subtensor(u_t[-self.noc:], outc_tm1) * 0.5
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(inb, W_inb))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb) )

            class_should_be = t_t[-self.noc:]
            outc_t = y_t[-self.noc:] *0.5 + class_should_be * 0.5

            # outc_t = TT.nnet.softmax((TT.dot(h_t, W_out) + TT.dot(outb, W_outb))[-self.noc:] )[0] + class_should_be * 0.5
            # y_t = TT.set_subtensor( y_t[-self.noc:], outc_t)
                
            return y_t, outc_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [y, outc], _ = theano.scan(step,
                                sequences=[self.u, self.t],
                                outputs_info=[self.u[0], self.outc0],
                                non_sequences=[self.W_in, self.W_inb, self.W_out, self.W_outb, self.outb, self.inb])#self.W, 
        return y,outc
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        y, outc = self._inner_step()
        
        # error between output and target
        error = TT.mean(((y - self.t) ** 2)) # .mean()
        # error = TT.nnet.categorical_crossentropy(y, self.t).mean()

        # gradients on the weights using BPTT
        gW_in, gW_inb, gW_out, gW_outb = TT.grad(error, [ self.W_in, self.W_inb, self.W_out, self.W_outb])#self.W,gW, 

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.outc0, self.index, self.indexto, self.lr, self.mom],
                        [error, 
                        y,
                        outc[-1]
                        ],
                        updates={
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_inb, self.mom*self.mW_inb - self.lr * gW_inb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_inb, self.W_inb + self.mW_inb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_out, self.W_out + self.mW_out)},
                        givens={(self.u, self.in_dtst[self.index : self.indexto]),
                                (self.t, self.out_dtst[self.index : self.indexto])}        
                        )
    def _net_trainer_accumulator_batch(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        y, outc = self._inner_step()
        
        # error between output and target
        error = TT.mean(((y - self.t) ** 2)) # .mean()
        # error = TT.nnet.categorical_crossentropy(y, self.t).mean()

        # gradients on the weights using BPTT
        gW_in, gW_inb, gW_out, gW_outb = TT.grad(error, [ self.W_in, self.W_inb, self.W_out, self.W_outb])#self.W,gW, 

        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.outc0, self.index, self.lr, self.mom],
                        [error, 
                        y,
                        outc[-1]
                        ],
                        updates={
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_inb, self.mom*self.mW_inb - self.lr * gW_inb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_inb, self.W_inb + self.mW_inb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_out, self.W_out + self.mW_out)},
                        givens={(self.u, self.in_dtst[self.index * self.batch_size: (self.index + 1) * self.batch_size]),
                                (self.t, self.out_dtst[self.index * self.batch_size: (self.index + 1) * self.batch_size])},
                        # profile=True
                        )

    def _net_step(self,):
        # make the inner step
        # y,outc = self._inner_step()
        def step(u_t,  W_in, W_inb, W_out, W_outb, outb, inb):#W, outc_tm1
            
            # u_t = TT.set_subtensor(u_t[-self.noc:], outc_tm1)
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(inb, W_inb))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb))
            
            outc_t = y_t[-self.noc:]

            return y_t, outc_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [y, outc], _ = theano.scan(step,
                                sequences=[self.u],
                                outputs_info=[None, None],#self.outc0
                                non_sequences=[self.W_in, self.W_inb, self.W_out, self.W_outb, self.outb, self.inb])#self.W, 
        
        # # calulate error
        # error = ((y - self.t) ** 2).sum()

        # compile and return the function
        return theano.function([ self.u],#self.outc0,
                        [y])

    def _load_weights(self, filename):
        import pickle

        folder = "trained_elman_out_rec/"

        f = open(folder+filename+".pickle","rb")

        net_w = pickle.load(f)
    
        self.W_in.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_inb.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[3].get_value()))


        f.close()