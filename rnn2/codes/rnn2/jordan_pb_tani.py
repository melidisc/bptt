import numpy as np
import theano
import theano.tensor as TT
import sys, os, pickle


class RNN_TANI_BPTT():
    def __init__(self, nh, ni, no, noc, pbs, bs, in_dtst, out_dtst):

        self.batch_size = bs
        
        self.in_dtst = theano.shared(in_dtst)
        self.out_dtst = theano.shared(out_dtst)

        self.index = TT.lscalar()
        self.indexto = TT.lscalar()


        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no
        # numbe of reccuring ouputs
        self.noc = noc
        #number of pb in input
        self.npb = pbs
        
        # input (where first dimension is time)
        self.u = TT.matrix(name="input")
        # target (where first dimension is time)
        self.t = TT.matrix(name="output")
        # target (where first dimension is time)
        self.outc = TT.matrix(name="context")
        
        # initial hidden state of the RNN
        self.h0 = TT.vector()
        # initial hidden state of the RNN
        self.outc0 = TT.vector(name="context_tm1")
        
        # learning rate
        self.lr = TT.scalar(name="learning_rate")
        # momentum rate
        self.mom = TT.scalar(name="momentum")
         # bias adaptation rate
        self.pb_lr = TT.scalar(name="learning_rate of parametric bias")

        
        # input bias
        self.inb = theano.shared(np.ones((1)).astype(theano.config.floatX))
        # output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))

        self.pb_v = theano.shared( np.random.random(self.npb).astype(theano.config.floatX))
        self.old_GW = theano.shared(np.zeros(self.npb))
        self.old_pb_vs = theano.shared( np.zeros((3,self.npb)).astype(theano.config.floatX))

        scale = 0.01
        # recurrent weights as a shared variable
        # self.W = theano.shared(scale*np.random.uniform(size=(self.n, self.n), low=-1, high=1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(scale*np.random.uniform(size=(self.npb+self.nin+self.noc, self.n), low=-1, high=1).astype(theano.config.floatX))
        # bias to hidden layer weights
        self.W_inb = theano.shared(scale*np.random.uniform(size=(1, self.n), low=-1, high=1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(scale*np.random.uniform(size=(self.n, self.nout+self.noc), low=-1, high=1).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(scale*np.random.uniform(size=(1, self.nout+self.noc), low=-1, high=1).astype(theano.config.floatX))

        # recurrent moemntum weights as a shared variable
        self.mW = theano.shared(np.zeros((self.n, self.n)).astype(theano.config.floatX))
        # input to hidden layer momentum
        self.mW_in = theano.shared(np.zeros((self.npb+self.nin+self.noc, self.n)).astype(theano.config.floatX))
        # input bias momentum
        self.mW_inb = theano.shared(np.zeros((1, self.n)).astype(theano.config.floatX))
        # hidden to output layer momentum
        self.mW_out = theano.shared(np.zeros((self.n, self.nout+self.noc)).astype(theano.config.floatX))
        # hidden to output bias momentum
        self.mW_outb = theano.shared(np.zeros((1, self.nout+self.noc)).astype(theano.config.floatX))
        # self.mW_outcb = theano.shared(np.zeros((1, self.noc)).astype(theano.config.floatX))

    
    def _inner_step(self,):
        
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, t_t, er_tm1, u_tm1, outc_tm1, W_in, W_inb, W_out, W_outb, outb, inb, pb_v):#W, 
            #closed loop output units reccurence
            u_t = TT.set_subtensor(u_t[:self.nin],  u_tm1[:self.nin] * 0.5 + u_t[:self.nin] *0.5)
            #context units reccurence
            u_t = TT.set_subtensor(u_t[-self.noc:], outc_tm1)
            #pb_v update with new value
            u_t = TT.set_subtensor(u_t[-(self.noc+self.npb):-self.noc], pb_v)
            #feed forward
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+ TT.dot(inb, W_inb))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb) )

            #keep the context
            outc_t = y_t[-self.noc:]
            #calculate the error
            error = er_tm1 + ((y_t[:self.nin] - t_t[:self.nin]) ** 2).mean()

            return error, y_t, outc_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [error, y, outc], _ = theano.scan(step,
                                sequences=[dict(input=self.u, taps=[-2,-1])],
                                outputs_info=[np.float64(.0), self.u[0][:self.nin+self.noc], self.outc0],
                                non_sequences=[self.W_in, self.W_inb, self.W_out, self.W_outb, self.outb, self.inb, self.pb_v])#self.W, 
        return error[-1], y,outc
    
    def _net_trainer_accumulator(self,):
        '''
        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in) + TT.nnet.sigmoid(TT.dot(h_tm1, W)))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=self.u,
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out])
        '''

        error, y, outc = self._inner_step()
        
        # error between output and target
        # bp_error = ((y[-self.nin:] - self.t[-self.nin:]) ** 2).sum()
        bp_error = error/ self.u.shape[0] #/(self.indexto-self.index)
        # error = TT.nnet.categorical_crossentropy(y, self.t).mean()

        # gradients on the weights using BPTT
        gW_in, gW_inb, gW_out, gW_outb = TT.grad(bp_error, [ self.W_in, self.W_inb, self.W_out, self.W_outb])#self.W,gW, 
        _gW_in, _gW_inb, _gW_out, _gW_outb = TT.grad(error, [ self.W_in, self.W_inb, self.W_out, self.W_outb])#self.W,gW, 
        #sum them
        tmp = 0.8 * TT.sum(_gW_in,1)[-(self.noc+self.npb):-self.noc] + 0.2* (self.old_pb_vs[0] - 2*self.old_pb_vs[1] + self.old_pb_vs[2])
        #update the bias
        delta = 0.5* tmp + 0.5* self.old_GW 
        self.pb_v = self.pb_v + delta#self.pb_v + 0.5 * tmp[-(self.noc+self.npb):-self.noc]#TT.nnet.sigmoid()

        self.old_pb_vs = TT.set_subtensor(self.old_pb_vs[2], self.old_pb_vs[1])
        self.old_pb_vs = TT.set_subtensor(self.old_pb_vs[1], self.old_pb_vs[0])
        self.old_pb_vs = TT.set_subtensor(self.old_pb_vs[0], self.pb_v)
        self.old_GW = tmp
        #calculate the gradient with the new bias
        # gW_in, gW_inb, gW_out, gW_outb = TT.grad(error, [ self.W_in, self.W_inb, self.W_out, self.W_outb])#self.W,gW, 
        # training function, that computes the error and updates the weights using
        # SGD.
        return theano.function([self.outc0, self.index, self.indexto, self.lr, self.mom],
                        [bp_error, y, outc[-1], TT.nnet.sigmoid(self.pb_v)],
                        updates={
                                (self.mW_in, self.mom*self.mW_in - self.lr * gW_in),
                                (self.mW_inb, self.mom*self.mW_inb - self.lr * gW_inb),
                                (self.mW_out, self.mom*self.mW_out - self.lr * gW_out),
                                (self.mW_outb, self.mom*self.mW_outb - self.lr * gW_outb),
                                (self.W_in, self.W_in + self.mW_in),
                                (self.W_inb, self.W_inb + self.mW_inb),
                                (self.W_outb, self.W_outb + self.mW_outb),
                                (self.W_out, self.W_out + self.mW_out)},
                        givens={(self.u, self.in_dtst[self.index : self.indexto]),
                                # (self.t, self.out_dtst[self.index * self.batch_size: (self.index + 1) * self.batch_size])
                                }        
                        )

    def _net_step(self,):
        # make the inner step
        # self.pb_v = theano.shared(0.0001* np.random.random(self.npb).astype(theano.config.floatX))
        error, y,outc = self._inner_step()
        
        # # calulate error
        # error = ((y[-self.noc:] - self.t[-self.noc:]) ** 2).sum()

        gW_in, gW_inb, gW_out, gW_outb = TT.grad(error, [ self.W_in, self.W_inb, self.W_out, self.W_outb])#self.W,gW, 
        
        tmp = 0.8 * TT.sum(gW_in,1)[-(self.noc+self.npb):-self.noc] + 0.2* (self.old_pb_vs[0] - 2*self.old_pb_vs[1] + self.old_pb_vs[2])
        #update the bias
        delta = 0.7* tmp + 0.3* self.old_GW 
        self.pb_v = self.pb_v + delta#self.pb_v + 0.5 * tmp[-(self.noc+self.npb):-self.noc]#TT.nnet.sigmoid()

        self.old_pb_vs = TT.set_subtensor(self.old_pb_vs[2], self.old_pb_vs[1])
        self.old_pb_vs = TT.set_subtensor(self.old_pb_vs[1], self.old_pb_vs[0])
        self.old_pb_vs = TT.set_subtensor(self.old_pb_vs[0], self.pb_v)
        self.old_GW = tmp

        # compile and return the function
        return theano.function([self.outc0, self.u],
                        [self.pb_v, y,error, outc[-1]])

    def _load_weights(self, filename):
        import pickle

        folder = "trained_elman_out_rec/"

        f = open(folder+filename+".pickle","rb")

        net_w = pickle.load(f)
    
        self.W_in.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_inb.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[3].get_value()))


        f.close()