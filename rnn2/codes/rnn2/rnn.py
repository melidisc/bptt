import numpy as np
import theano
import theano.tensor as TT
import sys, os, pickle
from elman_pb import RNN_BPTT

def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])


def make_train_set(dataset):
    '''
        returns 3 lists
        inputs: all the input series from all the movement sets, as ones
        bias: the paramteric bias trainig values
        outputs: creates the output targets for the input listss
    '''
    inputs = []
    bias = []
    outputs = []
    gestures_length = []
    for a_trial in dataset: 
        a_trial = _normalise(a_trial)

        for idx, gesture in enumerate(a_trial):
            gestures_length.append(len(gesture))
            inputs.extend(gesture.astype(theano.config.floatX))

            outputs.extend(
                np.append(gesture[1:],gesture[0]).reshape(gesture.shape).astype(theano.config.floatX)
                )
            
            pb = np.zeros(len(a_trial)+1)
            pb[-1] = 6.
            pb[idx] = 6.
            
            parametric_bias = np.tile(pb, 
                    gesture.shape[0]
                    ).reshape(gesture.shape[0], len(a_trial)+1).astype(theano.config.floatX)
            bias.extend(parametric_bias)

    print gestures_length
    return np.array(inputs), np.array(bias), np.array(outputs)


def net_train(net_trainer_accumulator, repetathions, batch_number, lr, mr):

    for _ in xrange(repetathions):
        error_acc = 0

        for idx in xrange(batch_number):
            what = net_trainer_accumulator(
                np.zeros(hiden_size).astype(theano.config.floatX), 
                idx,
                lr, 
                mr
                )
            error_acc += what[0]

        print "\tMSE ", error_acc
    print ""
   

def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array, filename):
    
    folder = "trained_elman_pb/"
    
    f = open(folder+"net_trained_"+filename+".pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()

def _normalise(targets):
    print "Normalising values ", len(targets), len(targets[0])
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss
    return targets

if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print "Usage "+sys.argv[0]+" [dirname] [extension] [file _load_weights] [file save_weights]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])

    out_size = 6
    in_size = 6
    pb_size = len(dtsets[0])+1
    hiden_size = 9
    batch_size = 40
    lr = 0.2#0.002
    mr = 0.5

    repetathions = 1000

    all_inputs, all_bias, all_outputs = make_train_set(dtsets[:-1])

    net = RNN_BPTT(hiden_size, in_size, out_size, pb_size, batch_size, all_inputs, all_bias, all_outputs )
    # net._load_weights()
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    print "NET CONF ",hiden_size, in_size, out_size, pb_size, lr, mr

    if sys.argv[3] == "no":
        print "Training"
        batch_number = all_inputs.shape[0] / batch_size
        net_train(net_trainer_accumulator, repetathions, batch_number, lr, mr)
    else:
        print "Whaaat"
        net._load_weights(sys.argv[3])
        import matplotlib.pyplot as plt
    
    
    targets = _normalise(dtsets[-1])
    target_out = []
    for t in targets:
        target_out.append(
            np.append(t[1:],t[0]).reshape(t.shape)
            )
    
    if sys.argv[4] != "no":
        save_weights([net.W, net.W_in, net.W_pb, net.W_out, net.W_outb], sys.argv[4])

    

    pb = np.random.uniform(size=len(targets)+1, low=-0.01, high=0.01)
    # pb = np.zeros(len(targets)+1)
    pb[-1] = 6.
    # plt.figure()
    succes_rate=np.zeros(len(targets))
    pbz_plot = []
    pbs_plot = []
    out_plot = []
    h = np.zeros(hiden_size).astype(theano.config.floatX)
    for idx, stuff in enumerate(zip(targets, target_out)):
        
        i, o =  stuff
        
        window_len = batch_size
        value_change = np.zeros(len(targets)+1)
        
        print "~~~~~~~~~~~~~~"
        
        for index in xrange(len(o)):
            
            start_point = index
            end_point = index+window_len
            if end_point > len(o):
                end_point = len(o)
            
            error = 0
            y= 0
            grad_W_pb_sum = 0

            for ___ in xrange(100):
                what = net_trainer_stepper(
                    h, 
                    i[start_point: end_point], 
                    np.tile(
                        pb,window_len).reshape(
                        window_len,pb.shape[0]),
                    o[start_point: end_point]
                    )
                
                error = what[0]
                grad_W_pb_sum= what[4]
                # print y[0], o[start_point: end_point], "\n"
                y= what[1]
                h= what[2]
                pb= what[3]
                    
                # import pdb;pdb.set_trace()
                
                h = h[0]
                pb = pb[0]
                grad_W_pb = np.sum(grad_W_pb_sum,1)

                pbz_plot.append(grad_W_pb)
                pbs_plot.append(1/(1 + np.power(np.e, -pb[:-1])))
                out_plot.append(y[0])

                
                    
                pb_buffer = pbz_plot[-window_len:] 

                pb1 = .5 * np.sum(pb_buffer,0)
                
                if len(pb_buffer)>2:
                    pb2 = 0. * (pb_buffer[-3] - 2*pb_buffer[-2] + pb_buffer[-1])
                else:
                    pb2 = 0.
                
                value_change = 0.5 * (pb1+pb2) + 0.6 * value_change
                
                pb[:-1] += value_change[:-1]
                # pb[:-1] = 1/ (1 + np.power(np.e, -pb[:-1]) )

                # print "pb1", pb1,    
                # print "pb2", pb2
                # print "value change ", value_change[:-1]
                print "error ", error,
                print ""+ str(idx) +" parametric bias ", np.argmax(pb[:-1]),
                print "gradient ", grad_W_pb 
                print "\r",

            if idx == np.argmax(pb[:-1]):
                succes_rate[idx]+=1

        print succes_rate, " of ", [len(t) for t in targets]
        if sys.argv[3] != "no":
            plt.figure()
            plt.ylabel('Values Seq'+str(idx))
            plt.xlabel('Time')
            plt.plot(pbs_plot[-len(o):],"--",label="Pb")
            # plt.plot(o,"-")
            # plt.legend()
            # plt.figure()
            plt.plot(out_plot,"o", label="output")
            # plt.legend()
    
    if sys.argv[3] != "no":
        plt.show()
    print idx


'''
def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array):
    
    f = open("net_trained_test.pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()


if __name__ == "__main__":
    
    if len(sys.argv) < 3:
        print "Usage "+sys.argv[0]+" [dirname] [extension]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])
    # f = open("6D_moves.pickle", "rb")
    # targets = pickle.load(f)
    # targets = targets
    out_size = 6
    in_size = 6
    pb_size = len(dtsets[0])+1
    hiden_size = 7

    repetathions = 50000

    net = RNN_BPTT(hiden_size, in_size, out_size, pb_size)
    # net._load_weights()
    net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    for targets in dtsets[:-1]:
        print "Normalising values ", len(targets), len(targets[0])
        # normalise values
        maxes = []
        mins = []
        for t in targets:
            maxes.append(np.amax(t, axis = 0) )
            mins.append(np.amin(t, axis = 0) )
        
        maxes = np.amax(maxes, axis = 0)
        mins = np.amin(mins, axis = 0)

        targetss = []
        for t in targets:
            for i, m in enumerate(zip(mins,maxes)):
                mi, ma = m
                if mi<0:
                    t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
                else:
                    t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
            targetss.append(t)
        targets = targetss

        maxes = []
        mins = []
        for t in targets:
            maxes.append(np.amax(t, axis = 0) )
            mins.append(np.amin(t, axis = 0) )
        
        maxes = np.amax(maxes, axis = 0)
        mins = np.amin(mins, axis = 0)



        print "NET CONF ",hiden_size, in_size, out_size, pb_size
        
        target_out = []
        for t in targets:
            target_out.append(
                np.append(t[1:],t[0]).reshape(t.shape)
                )
        plot_out=[]
        for _ in xrange(repetathions+10):
                
            print " Rep ", _,
            for idx, stuff in enumerate(zip(targets, target_out)):
                i, o =  stuff
                # print i[0].shape
                pb = np.zeros(len(targets)+1)
                pb[-1] = 6.
                pb[idx] = 6.
                what = net_trainer_accumulator(
                    np.zeros(hiden_size).astype(theano.config.floatX), 
                    i, 
                    np.tile(pb, 
                        o.shape[0]
                        ).reshape(o.shape[0], len(targets)+1),
                    o, 
                    0.021,
                    0.0
                    )
                
                print "\tMSE ", what[0], pb, #"\n output ", " ".join([str("\n "+str(x)+" | "+str(y)) for x,y in zip(what[1],o)]), #" \r",    
            print " "
            
            if _ > repetathions:
                # import matplotlib.pyplot as plt
                targets = dtsets[-1]
                
                save_weights([net.W, net.W_in, net.W_pb, net.W_out, net.W_outb])
                
                pb = np.random.uniform(size=len(targets)+1, low=-0.01, high=0.01)
                # pb = np.zeros(len(targets)+1)
                pb[-1] = 6.
                # plt.figure()
                succes_rate=np.zeros(len(targets))
                pbz_plot = []
                pbs_plot = []
                out_plot = []
                h = np.zeros(hiden_size).astype(theano.config.floatX)
                for idx, stuff in enumerate(zip(targets, target_out)):
                    
                    i, o =  stuff
                    
                    step = 30
                    value_change = np.zeros(len(targets)+1)
                    
                    print "~~~~~~~~~~~~~~"
                    
                    for index, al in enumerate(zip(i,o)):
                        
                        what = net_trainer_stepper(
                            h, 
                            np.tile(
                                al[0],step).reshape(
                                step,al[0].shape[0]), 
                            np.tile(
                                pb,step).reshape(
                                step,pb.shape[0]),
                            np.tile(
                                al[1],step).reshape(
                                step,al[1].shape[0])
                            )
                        
                        error, y, h, pb, grad_W_pb = what
                        
                        h = h[0]
                        pb = pb[0]
                        grad_W_pb = np.sum(grad_W_pb,1)

                        pbz_plot.append(grad_W_pb)
                        pbs_plot.append(1/(1 + np.power(np.e, -pb[:-1])))
                        out_plot.append(y[0])

                        if len(pbz_plot) > 33:
                            
                            pb_buffer = pbz_plot[-30:]

                            pb1 = 0.9 * (np.sum(pb_buffer,0)/ 30)
                            
                            pb2 = 0.4 * (pb_buffer[-3] - 2*pb_buffer[-2] + pb_buffer[-1])
                            
                            value_change = 2.5 * (pb1+pb2) + 0.5 * value_change
                            
                            pb[:-1] += value_change[:-1]
                            # pb[:-1] = 1/ (1 + np.power(np.e, -pb[:-1]) )

                            # print "pb1", pb1,    
                            # print "pb2", pb2
                            # print "value change ", value_change[:-1]
                            print "error ", error,
                            print ""+ str(idx) +" parametric bias ", pb[:-1], np.argmax(pb[:-1]),
                            # print "gradient ", grad_W_pb 
                            print "\r",
                        else:
                            print "not yet.."

                        if idx == np.argmax(pb[:-1]):
                            succes_rate[idx]+=1

                    print succes_rate, " of ", [len(t) for t in targets]

                    # plt.figure()
                    # plt.ylabel('Values Seq'+str(idx))
                    # plt.xlabel('Time')
                    # plt.plot(pbs_plot[-len(o):],"--",label="Pb")
                    # plt.plot(o,"-")
                    # plt.legend()
                    # plt.figure()
                    # plt.plot(out_plot,"-", label="output")
                    # plt.legend()
                
                # plt.show()
                print idx
                
                break
                 # for _ in xrange(repetathions):

    #     print " Rep ", _,
    #     for idx, stuff in enumerate(zip(inputs, bias, outputs)):
    #         i, b, o =  stuff
                   
            
    #         what = net_trainer_accumulator(
    #             np.zeros(hiden_size).astype(theano.config.floatX), 
    #             i.astype(theano.config.floatX), 
    #             b.astype(theano.config.floatX),
    #             o.astype(theano.config.floatX), 
    #             0.0005,
    #             0.0
    #             )
            
        #     print "\tMSE ", what[0], pb, #"\n output ", " ".join([str("\n "+str(x)+" | "+str(y)) for x,y in zip(what[1],o)]), #" \r",    
        # print " "
'''

          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
