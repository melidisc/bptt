import numpy as np
import theano
import theano.tensor as TT
import sys, os
import pickle
from elman_out_rec import RNN_BPTT_3 as RNN_BPTT
import time
import random
from matplotlib.font_manager import FontProperties
import matplotlib
matplotlib.rcParams.update({'font.size': 12})



def construct_inp(value, range):
    tmp = np.zeros(range)
    tmp[value] = 1
    return np.array([tmp])
def make_train_set(dataset):
    '''
        returns 3 lists
        inputs: all the input series from all the movement sets, as ones
        bias: the paramteric bias trainig values
        outputs: creates the output targets for the input listss
    '''
    inputs = []
    bias = []
    outputs = []
    gestures_length = [0]
    for a_trial in dataset: 
        a_trial = _normalise(a_trial)
        ####NOT SURE
        # random.shuffle(a_trial)
        ####NOT SURE
        for idx, gesture in enumerate(a_trial):
            gestures_length.append(len(gesture)+gestures_length[-1])
            
            tmp_cls = np.zeros(len(a_trial))

            _in = np.array([np.append(x, tmp_cls) for x in gesture])
            inputs.extend(_in.astype(theano.config.floatX))

            tmp_out = np.append(gesture[1:],gesture[0]).reshape(gesture.shape).astype(theano.config.floatX)
            tmp_cls[idx] = 1.

            _out = [np.append(x, tmp_cls) for x in tmp_out]
            outputs.extend(
                _out
                )
            
            # pb = np.zeros(len(a_trial))
            # pb[idx] = 6.
            
            # parametric_bias = np.tile(pb, 
            #         gesture.shape[0]
            #         ).reshape(gesture.shape[0], len(a_trial)).astype(theano.config.floatX)
            # bias.extend(parametric_bias)

    print gestures_length
    return np.array(inputs), np.array(outputs), gestures_length
def net_train(net_trainer_accumulator, repetathions, batch_number, lr, mr, idxs = None):
    check = False
    if idxs != None:
        check = True
        series = []
        for idx in range(1, len(idxs)):
            series.append( (idxs[idx-1],idxs[idx]))
    for _ in xrange(repetathions):
        error_acc = 0
        out_class = np.zeros(outc_size).astype(theano.config.floatX)
        
        if check:
            for s in series:
                what = net_trainer_accumulator(
                    out_class , 
                    s[0],#from
                    s[1],#to
                    lr, 
                    mr
                    )
                error_acc += what[0]
                out_class = what[2]
            random.shuffle(series)
        else:
            for idx in xrange(batch_number):
                what = net_trainer_accumulator(
                    out_class , 
                    idx,
                    lr, 
                    mr
                    )
                error_acc += what[0]
                out_class = what[2]
        if error_acc < 0.5:
            break
        if (_ % 50) == 0:
            print str(_) +" of "+str(repetathions)+" \t MSE ", error_acc #number of sequences
    print ""    

def read_dataset(dirname, extension):

    returnables = []

    for file in os.listdir(dirname):
        if file.endswith("."+extension):
            print(file)
            script_dir = os.path.dirname(__file__)

            rel_path = dirname+"/"+file

            abs_file_path = os.path.join(script_dir, rel_path)
    
            f = open(abs_file_path, "rb")
            # read
            returnables.append(pickle.load(f))
            # save and close
            f.close()
    return returnables

def save_weights(array, filename):
    
    folder = "trained_elman_out_rec/"

    f = open(folder+"net_cls3_trained_"+filename+".pickle", "wb")

    ar = np.array(array)

    pickle.dump(ar, f)

    f.close()
def _normalise(targets):
    print "Normalising values ", len(targets), [len(x) for x in targets]
    # normalise values
    maxes = []
    mins = []
    for t in targets:
        maxes.append(np.amax(t, axis = 0) )
        mins.append(np.amin(t, axis = 0) )
    
    maxes = np.amax(maxes, axis = 0)
    mins = np.amin(mins, axis = 0)

    targetss = []
    for t in targets:
        for i, m in enumerate(zip(mins,maxes)):
            mi, ma = m
            if mi<0:
                t[:,i] = ((t[:,i]+abs(mi)))/(ma+abs(mi))
            else:
                t[:,i] = ((t[:,i]-abs(mi)))/(ma-abs(mi))
        targetss.append(t)
    targets = targetss
    return targets

if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print "Usage "+sys.argv[0]+" [dirname] [extension] [file _load_weights] [file save_weights]"
        sys.exit(0)

    dtsets = read_dataset(sys.argv[1], sys.argv[2])

    out_size = 6
    in_size = 6
    outc_size = len(dtsets[0])
    hiden_size = int(sys.argv[5])
    batch_size = 40
    lr = 0.2 # 0.2
    mr = 0.300 # 0.30


    repetathions = 1400

    all_inputs, all_outputs, idxs = make_train_set(dtsets[:-1])


    net = RNN_BPTT(
        hiden_size, 
        in_size, 
        out_size, 
        outc_size, 
        batch_size, 
        all_inputs.astype(theano.config.floatX), 
        all_outputs.astype(theano.config.floatX)
        )

    if batch_size!=None:
        net_trainer_accumulator = net._net_trainer_accumulator_batch()
    else:
        net_trainer_accumulator = net._net_trainer_accumulator()
    net_trainer_stepper = net._net_step()

    print "NET CONF ",hiden_size, in_size, out_size, outc_size, lr, mr, batch_size

    if sys.argv[3] == "no":
        print "Training"
        if batch_size!=None:
            batch_number = all_inputs.shape[0] / batch_size
            net_train(net_trainer_accumulator, repetathions, batch_number, lr, mr, idxs = None)
        else:
            net_train(net_trainer_accumulator, repetathions, 0, lr, mr, idxs = idxs)
    else:
        print "Whaaat"
        net._load_weights(sys.argv[3])
        import matplotlib.pyplot as plt
    
    targets = _normalise(dtsets[-1])
    target_out = []
    for t in targets:
        target_out.append(
            np.append(t[1:],t[0]).reshape(t.shape)
            )

    if sys.argv[4] != "no":
        save_weights(
            [net.W_in, 
            net.W_inb, 
            net.W_out, 
            net.W_outb],  
            sys.argv[4]
            )

    
    succes_rate=np.zeros(len(targets))
    error_rate=np.zeros(len(targets))
    
    pbz_plot = []
    pbs_plot = []
    out_plot = []
    
    h = np.zeros(hiden_size).astype(theano.config.floatX)
    outc = np.zeros(outc_size).astype(theano.config.floatX)
    what_else=[]
    #test on training set
    for idx, elem in enumerate(zip(all_inputs, all_outputs)):
        inp, outp = elem
        inp[-outc_size:]= outc
        what = net_trainer_stepper([inp.astype(theano.config.floatX)])
        what = what[0][0]

        outc = what[-outc_size:]
        # print outc
        if np.argmax(outp[-outc_size:]) == np.argmax(outc):
            succes_rate[np.argmax(outp[-outc_size:])] +=1
        else:
            error_rate[np.argmax(outp[-outc_size:])] +=1
            what_else.append((outc, np.argmax(outp[-outc_size:]))) 
        # print np.argmax(outp[-outc_size:]),
        y = what[:-outc_size]
    
    print "IN TRAINING SET, succes_rate",succes_rate,"error_rate", error_rate
    outc = np.zeros(outc_size).astype(theano.config.floatX)
    succes_rate=np.zeros(len(targets))
    error_rate=np.zeros(len(targets))
    
    #real test
    for idx, stuff in enumerate(zip(targets, target_out)):

        i, o =  stuff
        class_plot = []    
        out_plot = []
        
        for elem in zip(i,o):
            outc = np.zeros(outc_size).astype(theano.config.floatX)
            inp, outp = elem
            inp = np.append(inp, outc)
            outp = np.append(outp, np.zeros(outc_size))
            
            # for __ in xrange(3):
            what = net_trainer_stepper([inp.astype(theano.config.floatX)])
            what = what[0][0]

            outc = what[-outc_size:]
            # print np.argmax(outc), idx
            if idx == np.argmax(outc):
                succes_rate[idx] +=1
            else:
                error_rate[idx] +=1
                what_else.append((outc, idx)) 

            y = what[:-outc_size]

            out_plot.append(y)
            class_plot.append(outc)

        if sys.argv[3] != "no":
            plt.figure()
            plt.plot(out_plot,"--",label="net output")
            plt.plot(o,label="real output")
            
            plt.figure()
            plt.suptitle("Context Calssification "+str(idx))
            class_plot = np.array(class_plot)
            for theshit in xrange(len(class_plot[0])): 
                plt.plot(class_plot[:,theshit],"-",label="Class "+str(theshit))
            plt.legend(loc=2,prop={'size':6})
            timestr = time.strftime("%Y%m%d-%H%M%S")
            plt.ylim([-0.1, 1.1])
            plt.xlim([-10, 600])
            # plt.show()
            # plt.savefig('figures/'+sys.argv[0]+timestr+'.png',figsize=(11,8),dpi=600,bbox_inches='tight')
    print "IN TEST SET, succes_rate",succes_rate,"error_rate", error_rate
    # print what_else
    # plt.show()

        # ii = []
        # for inn in i:
        #     ii.append(
        #         np.append(inn, np.zeros(len(targets)))
        #         )
        # i = np.array(ii)
        # pb = np.zeros(len(targets))
        # pb[idx] = 1

        # oo = []
        # for outt in o:
        #     oo.append(
        #         np.append(outt, pb)
        #         )
        # o = np.array(oo)
        
        
    #     y = what[0]
        
    #     print "classifier ", y[-1][-outc_size:]

    #     pbs_plot=y[:][-outc_size:]
    #     out_plot=y[:][:outc_size]
    #     # print "error", error
    #     # print succes_rate, " of ", [len(t) for t in targets]

    #     if sys.argv[3] != "no":
    #         plt.figure()
    #         plt.ylabel('Values Seq'+str(idx))
    #         plt.xlabel('Time')
    #         plt.plot(pbs_plot,"--",label="Pb")
    #         plt.plot(o,"-")
    #         plt.legend()
    #         plt.figure()
    #         plt.plot(out_plot,"-", label="output")
    #         plt.legend()
    
    # if sys.argv[3] != "no":
    #     plt.show()
    # print idx
    


          

# Rep  254   MSE  391.456760236  MSE  419.235495495  MSE  176.470080473  

# net_w = pickle.load(open("net_trained.pickle","rb"))
# In [10]: net.W, net.W_in, net.W_pb, net.W_out, net.W_outb = net_w[0]
'''
    for _ in xrange(repetathions):
        
        for targets in dtsets[:-1]:
            
            targets = _normalise(targets)

            target_out = []
            for t in targets:
                target_out.append(
                    np.append(t[1:],t[0]).reshape(t.shape)
                    )
            
                
            print " Rep ", _,
            

            ov_er = 0
            for idx, stuff in enumerate(zip(targets, target_out)):
                h = np.zeros(hiden_size).astype(theano.config.floatX)
                outc = np.zeros(outc_size).astype(theano.config.floatX)
                i, o =  stuff

                pb = np.zeros(len(targets))
                pb[idx] = 1

                oo = []
                for outt in o:
                    oo.append(
                        np.append(outt, pb)
                        )
                o = np.array(oo)
                ii = []
                for inn in i:
                    ii.append(
                        np.append(inn, np.zeros(len(targets)))
                        )
                i = np.array(ii)
                # pb = np.tile(pb, i.shape[0]).reshape(i.shape[0], len(targets))

                what = net_trainer_accumulator(
                    h.astype(theano.config.floatX),
                    outc.astype(theano.config.floatX), 
                    i.astype(theano.config.floatX), 
                    o.astype(theano.config.floatX),
                    0.00081,
                    0.0
                    )
                outc = what[-2]
                h = what[-1]
                # print "\nMSE _",idx,"_", what[0], " \n", outc, pb
                ov_er+= what[0]

            print "Overal error ", ov_er
    '''